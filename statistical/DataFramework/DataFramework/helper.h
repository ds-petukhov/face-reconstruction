﻿#pragma once

#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <vector>
#include <math.h>

using namespace std;


struct mvec {
	vec3<float> point;
	int num;
};


struct mvec find_closest(vector<vec3<float>> vertices, vec3<float> point)
{
	float distance = 1000000.f;
	vec3<float> tmp;
	int k;
	for (int i = 0; i < vertices.size(); i++)
	{
		vec3<float> cur = vertices[i];
		float d = sqrt(pow(cur.x - point.x, 2.0) + pow(cur.y - point.y, 2.0) + pow(cur.z - point.z, 2.0));
		if (d < distance)
		{
			distance = d;
			tmp = cur;
			k = i;
		}
	}
	cout << distance << endl;

	//TO DO: добавить пропуск ненайденных 3д точек

	struct mvec temp;
	temp.point = tmp;
	temp.num = k;
	return temp;
}

pair<vector<vec3<float>>, vector<vec3<float>>> getBMFConsts(string fileConstX0, string fileConstD)
{
	pair<vector<vec3<float>>, vector<vec3<float>>> returnVectors;

	vector<vec3<float>> constX0;
	vector<vec3<float>> constD;

	ifstream constfile;
	float x, y, z;

	constfile.open(fileConstX0);

	while (constfile >> x >> y >> z)
	{
		vec3<float> pos = { x, y, z };
		constX0.push_back(pos);
	}

	constfile.close();


	constfile.open(fileConstD);

	while (constfile >> x >> y >> z)
	{
		vec3<float> pos = { x, y, z };
		constD.push_back(pos);
	}

	constfile.close();

	returnVectors.first = constX0;
	returnVectors.second = constD;

	return returnVectors;
}


void printFormatedStat(string alphafilename, string statfilename, 
	vector<vec2<float>> landmarks,
	vector<double> matR,
	vector<double> vecT,
	int width, int height,
	float focal,
	double noiseLevel)
{
	//Читаем настоящую альфа из файла после генерации лица в matlab

	ifstream alphafile;
	int alpha_dim = 0;
	alphafile.open(alphafilename);
	
	float x;
	vector<float> alpha;
	while (alphafile >> x)
	{
		alpha.push_back(x);
		alpha_dim++;
	}

	alphafile.close();

	//Оформляем статистические данные в требуемом формате

	ofstream statfile;
	statfile.open(statfilename);

	////////

	statfile << "resolution\n";
	char resolutionoutput[100];
	sprintf(resolutionoutput, "%d %d\n", width, height);
	statfile << resolutionoutput;

	////////

	statfile << "noise level\n";
	char noiseoutput[100];
	sprintf(noiseoutput, "%.1f\n", noiseLevel);
	statfile << noiseoutput;

	////////

	statfile << "focal length\n";
	char tanoutput[100];
	sprintf(tanoutput, "%.6f\n", focal);
	statfile << tanoutput;

	////////

	statfile << "landmarks count\n";

	int landmarks_count = landmarks.size();
	ostringstream temp;
	temp << landmarks_count;
	statfile << temp.str() + "\n";

	////////

	statfile << "landmarks\n";

	for (int j = 0; j < landmarks_count; j++)
	{
		char pointoutput[100];
		sprintf(pointoutput, "%.2f %.2f\n", landmarks[j].x, landmarks[j].y);
		statfile << pointoutput;
	}

	////////
	/*
	statfile << "projections\n";

	for (int j = 0; j < landmarks_count; j++)
	{
	char pointoutput[100];
	sprintf(pointoutput, "%.6f %.6f %.6f\n", projections[j].point.x, projections[j].point.y, projections[j].point.z);
	statfile << pointoutput;
	}
	*/
	////////

	statfile << "alpha dim\n";

	ostringstream tempstr;
	tempstr << alpha_dim;
	statfile << tempstr.str() + "\n";

	////////

	statfile << "truth alpha\n";

	for (int j = 0; j < alpha_dim; j++)
	{
		char pointoutput[100];
		sprintf(pointoutput, "%.12f\n", alpha[j]);
		statfile << pointoutput;
	}

	////////

	statfile << "truth rotation\n";

	int matrix_dim = matR.size();

	for (int j = 0; j < matrix_dim - 1; j++)
	{
		char pointoutput[100];
		sprintf(pointoutput, "%.12f ", matR[j]);
		statfile << pointoutput;
	}

	char bufferoutput[100];
	sprintf(bufferoutput, "%.12f\n", matR[matrix_dim - 1]);
	statfile << bufferoutput;

	////////

	statfile << "truth translation\n";

	char stroutput[100];
	sprintf(stroutput, "%.12f %.12f %.12f\n", vecT[0], vecT[1], vecT[2]);
	statfile << stroutput;

	////////

	statfile.close();
}
