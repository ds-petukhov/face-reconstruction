#ifndef __RENDER_H__
#define __RENDER_H__

#include <vector>

using namespace std;

class Render {

public:
	Render(int width, int height, float fov);
	~Render();

	void renderScene();
	void openWindow();
	void getScene(string modelpath);
	void releaseScene();
	void saveRender(string renderOutputPath);
	void swapBuffers();

	void coloredRender(bool color);
	void randomCameraPose(float angle, float trans);
	void setWindowSize(int width, int height);

	vec3<float> get3DPoint(vec2<float> point);
	void addGaussianNoise(string renderoutput, double m_NoiseStdDev);
	void reverseTest(vec3<float> point, string landmarksoutput);

	vector<double> getR();
	vector<double> getT();
	vector<double> getFocals();
	
	vector<vec3<float>> vertices;
	

	
private:

	void createWindow();
	
};


#endif //__RENDER_H__

