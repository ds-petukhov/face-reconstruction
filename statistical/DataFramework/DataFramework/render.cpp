﻿
#include "stdafx.h"

#include "render.h"

#include <stdlib.h>
#include <iostream>
#include <fstream>

#include <stdlib.h>
#include <stdio.h>
#include <sstream>

#include <cmath>
#include <algorithm>
#include <string>
#include <string.h>



#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>



#include <GL/glew.h>
#ifdef __APPLE__
#include <freeglut.h>
#else
#include <GL/freeglut.h>
#endif


#define GL_BGR 0x80E0 

#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

int win;
const aiScene* scene = NULL;

aiVector3D scene_min, scene_max, scene_center;

#define aisgl_min(x,y) (x<y?x:y)
#define aisgl_max(x,y) (y>x?y:x)

#define MODEL_SCALE 10000.0

vector<vec3<float>> vertex_vector;
vector<vec3<float>> normals_vector;
vector<rgb<float>> color_vector;

float fieldOfView;

bool colored_render;

float camera_angle;
float camera_trans;

int window_width;
int window_height;

float zNear;
float zFar;

GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };  /* Infinite light location. */


void Render::addGaussianNoise(string renderoutput, double m_NoiseStdDev)
{
	Mat source;
	source = imread(renderoutput, 1);

	Mat result = Mat(source.size(), CV_8UC3);
	Mat mGaussian_noise = Mat(source.size(), CV_16SC3);

	randn(mGaussian_noise, 0, m_NoiseStdDev);

	for (int Rows = 0; Rows < source.rows; Rows++)
	{
		for (int Cols = 0; Cols < source.cols; Cols++)
		{
			Vec3b Source_Pixel = source.at<Vec3b>(Rows, Cols);
			Vec3b &Des_Pixel = result.at<Vec3b>(Rows, Cols);
			if ((abs(Source_Pixel.val[0] - 254) < 20) && (abs(Source_Pixel.val[1] - 249) < 20) && (abs(Source_Pixel.val[2] - 248)) < 20)
			{
				for (int i = 0; i < 3; i++)
				{
					Des_Pixel.val[i] = Source_Pixel.val[i];
				}
			}
			else
			{
				Vec3s Noise_Pixel = mGaussian_noise.at<Vec3s>(Rows, Cols);

				for (int i = 0; i < 3; i++)
				{
					Des_Pixel.val[i] = Source_Pixel.val[i] + Noise_Pixel.val[i];
				}
			}


		}
	}

	//string noisedoutput = "data/renders/rhead1noised.jpg";
	cv::imwrite(renderoutput, result);
}


/* ---------------------------------------------------------------------------- */
void get_bounding_box_for_node(const aiNode* nd,
	aiVector3D* min,
	aiVector3D* max,
	aiMatrix4x4* trafo
	){
	aiMatrix4x4 prev;
	unsigned int n = 0, t;

	prev = *trafo;
	aiMultiplyMatrix4(trafo, &nd->mTransformation);

	for (; n < nd->mNumMeshes; ++n) {
		const aiMesh* mesh = scene->mMeshes[nd->mMeshes[n]];
		for (t = 0; t < mesh->mNumVertices; ++t) {

			aiVector3D tmp = mesh->mVertices[t];
			aiTransformVecByMatrix4(&tmp, trafo);

			min->x = aisgl_min(min->x, tmp.x);
			min->y = aisgl_min(min->y, tmp.y);
			min->z = aisgl_min(min->z, tmp.z);

			max->x = aisgl_max(max->x, tmp.x);
			max->y = aisgl_max(max->y, tmp.y);
			max->z = aisgl_max(max->z, tmp.z);
		}
	}

	for (n = 0; n < nd->mNumChildren; ++n) {
		get_bounding_box_for_node(nd->mChildren[n], min, max, trafo);
	}
	*trafo = prev;
}

/* ---------------------------------------------------------------------------- */
void get_bounding_box(aiVector3D* min, aiVector3D* max)
{
	aiMatrix4x4 trafo;
	aiIdentityMatrix4(&trafo);

	min->x = min->y = min->z = 1e10f;
	max->x = max->y = max->z = -1e10f;
	get_bounding_box_for_node(scene->mRootNode, min, max, &trafo);
}


/* ---------------------------------------------------------------------------- */
void reshape(int width, int height)
{
	const double aspectRatio = (float)width / height;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(fieldOfView, aspectRatio, zFar - zNear, zFar + 2.5*(zFar - zNear));  /* Znear and Zfar */
	
	glViewport(0, 0, width, height);
}


/* ---------------------------------------------------------------------------- */
void displayRandomPose(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	//gluLookAt(0.f, 0.f, 1.5, 0.f, 0.f, -5.f, 0.f, 1.f, 0.f);
	/* scale the whole asset to fit into our view frustum */
	/*
	float tmp;
	tmp = scene_max.x - scene_min.x;
	tmp = aisgl_max(scene_max.y - scene_min.y, tmp);
	tmp = aisgl_max(scene_max.z - scene_min.z, tmp);
	tmp = 1.f / tmp;
	glScalef(tmp, tmp, tmp);
	*/

	/* center the model */
	//glTranslatef(-scene_center.x, -scene_center.y, -scene_center.z);

	glTranslatef(0, 0, -2.5*(zFar - zNear));


	//add random rotation and translation
	glTranslatef(0.0f, 0.0f, camera_trans);
	glRotatef(camera_angle, 0.f, 1.f, 0.f);

	cout << colored_render << endl;

	for (int i = 0; i < vertex_vector.size(); i = i + 3)
	{
		glBegin(GL_TRIANGLES);

		for (int j = 0; j < 3; j++)
		{
			if (colored_render)
			{
				const GLfloat color[3] = { color_vector[i + j].r, color_vector[i + j].g, color_vector[i + j].b };
				glColor3fv(color);
			}
			else 
			{
				const GLfloat normal[3] = { normals_vector[i + j].x, normals_vector[i + j].y, normals_vector[i + j].z };
				glNormal3fv(normal);
				//разобраться почему просто заливка одним цветом (добавить освещение)
				const GLfloat color[3] = { 100.f / 255, 90.f / 255, 110.f / 255 };
				glColor3fv(color);
			}

			const GLfloat vertex[3] = { vertex_vector[i + j].x, vertex_vector[i + j].y, vertex_vector[i + j].z };
			glVertex3fv(vertex);
		}
		glEnd();
	}
}

void Render::createWindow()
{
	win = glutCreateWindow("Render Face");
	//glutDisplayFunc(display);
	glutDisplayFunc(displayRandomPose);
	glutReshapeFunc(reshape);

	glutIdleFunc(displayRandomPose);

	//glClearColor(248.f / 255, 248.f / 255, 255.f / 255, 1.f);
	glClearColor(255.f / 255, 255.f / 255, 255.f / 255, 1.f);
	
	
	/*
	if (!colored_render)
	{
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
	}
	*/

	//glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	
	cout << colored_render << endl;
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	
	
	if (colored_render)
	{
		glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
	}


	//glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHTING);    // Uses default lighting parameters

	
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	
	glEnable(GL_NORMALIZE);

	/* XXX docs say all polygons are emitted CCW, but tests show that some aren't. */
	if (getenv("MODEL_IS_BROKEN"))
		glFrontFace(GL_CW);

	//glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
	glutGet(GLUT_ELAPSED_TIME);

	//glutMainLoop();

	//glutMainLoopEvent();

	

}


void Render::reverseTest(vec3<float> point, string landmarksoutput)
{
	GLint viewport[4];
	GLdouble modelview[16];
	GLdouble projection[16];
	GLdouble winX, winY, winZ;

	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glGetIntegerv(GL_VIEWPORT, viewport);

	gluProject(point.x, point.y, point.z,
				modelview, projection, viewport,
				&winX, &winY, &winZ);

	winY = (float)viewport[3] - (float)winY;

	Mat image = cv::imread(landmarksoutput);
	cv::circle(image, cv::Point2f(winX, winY), 1, cv::Scalar(255.0, 0.0, 0.0));
	cv::imwrite(landmarksoutput, image);
}

vector<double> Render::getR()
{
	GLdouble modelview[16];
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	vector<double> v;
	//reverse y and z coord
	for (const auto x : { modelview[0], modelview[4], modelview[8], -modelview[1], -modelview[5], -modelview[9], -modelview[2], -modelview[6], -modelview[10] })
		v.push_back(x);
	return v;
}

vector<double> Render::getT()
{
	GLdouble modelview[16];
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	vector<double> v;
	//revrse y and z coord
	for (const auto x : { modelview[12], -modelview[13], -modelview[14]})
		v.push_back(x);
	return v;
}

vector<double> Render::getFocals()
{
	GLdouble projection[16];
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	vector<double> v;
	for (const auto x : { projection[0], projection[5] })
		v.push_back(x);
	return v;
}

vec3<float> Render::get3DPoint(vec2<float> point)
{
	GLint viewport[4];
	GLdouble modelview[16];
	GLdouble projection[16];
	GLfloat winX, winY, winZ;
	GLdouble posX, posY, posZ;

	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glGetIntegerv(GL_VIEWPORT, viewport);


	
	winX = (float)point.x;
	winY = (float)viewport[3] - (float)point.y;
	glReadPixels(point.x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

	gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

	//float farz = 3.0*zFar;
	//float nearz = -1.0*zNear;
	//float zbufWinz = (-2.0 * farz * nearz / (farz - nearz)) / (winZ - (nearz + farz) / (farz - nearz));

	//TO DO: добавить проверку 3д точки, что она на модели

	vec3<float> v = { (float)posX, (float)posY, (float)posZ };

	return v;
}

void Render::saveRender(string renderOutputPath)
{
	int height = glutGet(GLUT_WINDOW_HEIGHT);
	int width = glutGet(GLUT_WINDOW_WIDTH);

	Mat pixels(height, width, CV_8UC3);
	//use fast 4-byte alignment (default anyway) if possible
	glPixelStorei(GL_PACK_ALIGNMENT, (pixels.step & 3) ? 1 : 4);
	//set length of one complete row in destination data (doesn't need to equal img.cols)
	glPixelStorei(GL_PACK_ROW_LENGTH, pixels.step / pixels.elemSize());
	glReadPixels(0, 0, pixels.cols, pixels.rows, GL_BGR, GL_UNSIGNED_BYTE, pixels.data);
	
	Mat flipped(height, width, CV_8UC3);
	flip(pixels, flipped, 0);
	imwrite(renderOutputPath, flipped);
	
}

void Render::swapBuffers()
{
	glutSwapBuffers();
}

Render::~Render()
{
	
}

void Render::renderScene()
{
	//win = glutCreateWindow("Render Face");
	//createWindow();

	glutMainLoopEvent();
}

void Render::releaseScene()
{
	glutDestroyWindow(win);

	/* cleanup - calling 'aiReleaseImport' is important, as the library
	keeps internal resources until the scene is freed again. Not
	doing so can cause severe resource leaking. */
	aiReleaseImport(scene);

	/* We added a log stream to the library, it's our job to disable it
	again. This will definitely release the last resources allocated
	by Assimp.*/
	aiDetachAllLogStreams();

	scene = NULL;
	color_vector.clear();
	vertex_vector.clear();
	normals_vector.clear();
	vertices.clear();

}

void Render::getScene(string scenepath)
{
	zNear = 0.0;
	zFar = 0.0;
	aiLogStream stream;

	/* get a handle to the predefined STDOUT log stream and attach
	it to the logging system. It remains active for all further
	calls to aiImportFile(Ex) and aiApplyPostProcessing. */
	stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, NULL);
	aiAttachLogStream(&stream);

	stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT, NULL);
	aiAttachLogStream(&stream);

	/* we are taking one of the postprocessing presets to avoid
	spelling out 20+ single postprocessing flags here. */
	scene = aiImportFile(scenepath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);

	if (scene) {
		get_bounding_box(&scene_min, &scene_max);
		scene_center.x = (scene_min.x + scene_max.x) / 2.0f;
		scene_center.y = (scene_min.y + scene_max.y) / 2.0f;
		scene_center.z = (scene_min.z + scene_max.z) / 2.0f;
		
		zNear = scene_min.z;
		zFar = scene_max.z;
		//zNear = scene_min.z / MODEL_SCALE;
		//zFar = scene_max.z / MODEL_SCALE;
	}
	

	const aiMesh* mesh = scene->mMeshes[scene->mRootNode->mMeshes[0]];

	for (int t = 0; t < mesh->mNumFaces; ++t) {
		const aiFace* face = &mesh->mFaces[t];

		for (int i = 0; i < face->mNumIndices; i++) {
			int index = face->mIndices[i];
			rgb<float> color = { mesh->mColors[0][index][0], mesh->mColors[0][index][1], mesh->mColors[0][index][2] };
			color_vector.push_back(color);
			
			vec3<float> norm = { (float)mesh->mNormals[index].x, (float)mesh->mNormals[index].y, (float)mesh->mNormals[index].z };
			normals_vector.push_back(norm);

			vec3<float> pos = { (float)mesh->mVertices[index].x, (float)mesh->mVertices[index].y, (float)mesh->mVertices[index].z};
			vertex_vector.push_back(pos);
		}

	}

	for (int t = 0; t < mesh->mNumVertices; ++t) {
		aiVector3D tmp = mesh->mVertices[t];
		vec3<float> v = { tmp.x, tmp.y, tmp.z };
		vertices.push_back(v);
	}

	/*
	float tmp_zNear = 0.0;
	float tmp_zFar = 0.0;

	for (int i = 0; i < vertices.size(); ++i)
	{
		if (vertices[i].z < tmp_zNear)
		{
			tmp_zNear = vertices[i].z;
		}
		if (vertices[i].z > tmp_zFar)
		{
			tmp_zFar = vertices[i].z;
		}
	}

	zNear = tmp_zNear;
	zFar = tmp_zFar;
	*/
}

void Render::openWindow()
{
	createWindow();
}

void Render::coloredRender(bool color)
{
	colored_render = color;
}

void Render::randomCameraPose(float angle, float trans)
{
	camera_angle = angle;
	camera_trans = trans;
}

void Render::setWindowSize(int width, int height)
{
	window_width = width;
	window_height = height;
	glutInitWindowSize(width, height);
}

Render::Render(int width, int height, float fov)
{

	char *myargv[1];
	int myargc = 1;
	myargv[0] = _strdup("appname");

	fieldOfView = fov;
	colored_render = true;
	float camera_angle = 0.0;
	float camera_trans = 0.0;

	glutInitWindowSize(width, height);
	//glutInitDisplayMode(GLUT_SINGLE);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInit(&myargc, myargv);

};