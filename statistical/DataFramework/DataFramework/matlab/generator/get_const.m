[model msz] = load_model();

load ../04_attributes.mat

tmp = [1.0; zeros(msz.n_shape_dim - 1, 1)];

n_seg = size(tmp, 2);
n_dim = size(tmp, 1);

X0 = model.shapeMU*ones([1 n_seg]);
D = model.shapePC(:,1:n_dim) * (tmp .* (model.shapeEV(1:n_dim)*ones([1 n_seg])) )  ;

[m,n] = size(X0);

nver = m/3;


[fid, message] = fopen('../../data/pts_stat/constx0.txt', 'w');
for i=1:nver
  fprintf(fid, '%f %f %f\n', single(X0(3*(i-1)+1:3*i)) );
end
fclose(fid);

[fid, message] = fopen('../../data/pts_stat/constD.txt', 'w');
for i=1:nver
  fprintf(fid, '%f %f %f\n', single(D(3*(i-1)+1:3*i)) );
end
fclose(fid);