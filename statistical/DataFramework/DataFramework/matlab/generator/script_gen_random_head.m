%Generate a random head, render it and export it to PLY file

[model msz] = load_model();

load ../04_attributes.mat


i = 0;

    disp(strcat('Creating-', num2str(i),'-model...'));
    
    %param = normrnd(0,sqrt(1.0e+005*8.8434));
    
    % param = randn;
    % a = [param; zeros(msz.n_shape_dim - 1, 1)];
    
    a = zeros(msz.n_shape_dim, 1);
    b = zeros(msz.n_tex_dim, 1);
    
    shape = coef2object( a, model.shapeMU, model.shapePC, model.shapeEV );
    tex   = coef2object( b, model.texMU,   model.texPC,   model.texEV );
    
    ptsname = strcat('../../data/basel/alpha/rh', num2str(i),'.txt');

    [fid, message] = fopen(ptsname, 'w');
    % fprintf(fid,'%f\n', param);
    for j=1:msz.n_shape_dim
        fprintf(fid,'%f ', a(j));
    end
    fprintf(fid, '\n');
    fclose(fid);
    
    filename = strcat('../../data/basel/rh', num2str(i),'.ply');
    plywrite(filename, shape, tex, model.tl );






