#ifndef __DETECTOR_H__
#define __DETECTOR_H__


#include <string>
#include <string.h>
#include <vector>

using namespace std;

class Detector {

public:
	Detector();
	~Detector();
	vector<vec2<float>> detectLandmarks(string input, string output);
	vector<vec2<float>> detectLandmarksCI2CV(string input, string output);
};


#endif //__DETECTOR_H__

