// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <stdio.h>
#include <tchar.h>

template <typename T>
struct vec3 {
	T x;
	T y;
	T z;
};

template <typename T>
struct vec2 {
	T x;
	T y;
};

template <typename T>
struct rgb {
	T r;
	T g;
	T b;
};

// TODO: reference additional headers your program requires here
