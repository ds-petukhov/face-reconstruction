﻿//DiplomaProject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "render.h"
#include "detector.h"
#include "helper.h"

#include <Windows.h>
#include <process.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <vector>
#include <math.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

string modelname     = "rhead";
string basel_folder  = "data/exp/";
string exp_folder    = "data/exp/";
string render_folder = "renders/";
string mean_folder   = "mean/";

//#include "engine.h"

void getModelStat()
{
	vector<vec3<float>> constX0;
	vector<vec3<float>> constD;
	
	pair<vector<vec3<float>>, vector<vec3<float>>> returnVectors = getBMFConsts("data/pts_stat/constx0.txt", "data/pts_stat/constD.txt");

	constX0 = returnVectors.first;
	constD = returnVectors.second;
	
	Render* glrender = new Render(800,600,45.0);
	Detector* detector = new Detector();

	for (int i = 2089; i <= 2089; i++)
	{
		ofstream statfile;

		string stri;
		ostringstream temp;
		temp << i;
		stri = temp.str();

		string statfilename = "data/pts_stat/rhead" + stri + ".txt";
		statfile.open(statfilename, ofstream::app);


		string modelname = "D:/diploma/cov/basel/rhead" + stri + ".ply";
		string renderoutput = "data/renders/rhead" + stri + ".jpg";
		string landmarksoutput = "data/landmarks/rhead" + stri + ".jpg";

		float angle = rand() % 60 - 30.f;
		float trans = rand() % 40000 - 20000.f;

		glrender->getScene(modelname);
		glrender->coloredRender(true);
		glrender->randomCameraPose(angle, trans);
		glrender->renderScene();
		glrender->saveRender(renderoutput);

		vector<double> matR = glrender->getR();
		for (int j = 0; j < matR.size(); j++)
		{
			char pointoutput[100];
			sprintf(pointoutput, "%.10f ", matR[j]);
			statfile << pointoutput;
		}
		statfile << "\n";

		vector<double> vecT = glrender->getT();
		char pointoutput[100];
		sprintf(pointoutput, "%.10f %.10f %.10f\n", vecT[0], vecT[1], vecT[2]);
		statfile << pointoutput;
		
		vector<vec2<float>> landmarks = detector->detectLandmarks(renderoutput, landmarksoutput);

		if (remove(renderoutput.data()) != 0)
			perror("Error deleting file");

		vector<vec3<float>> vertices = glrender->vertices;

		for (int j = 0; j < landmarks.size(); j++)
		{
			vec3<float> point3d = glrender->get3DPoint(landmarks[j]);

			struct mvec p3d = find_closest(vertices, point3d);

			glrender->reverseTest(p3d.point, landmarksoutput);
						
			char pointoutput[300];

			sprintf(pointoutput, "%d %.2f %.2f %d %.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f\n",
			j + 1, landmarks[j].x, landmarks[j].y,
			p3d.num, p3d.point.x, p3d.point.y, p3d.point.z,
			constX0[p3d.num].x, constX0[p3d.num].y, constX0[p3d.num].z,
			constD[p3d.num].x, constD[p3d.num].y, constD[p3d.num].z);

			statfile << pointoutput;
			
		}

		glrender->swapBuffers();
		glrender->releaseScene();

		statfile.close();

	}

	delete glrender;
	delete detector;

	cout << "FINISHED!" << endl;


}

void calcCovMat(int N_MODELS, int N_LANDMARKS)
{
	Mat cov = Mat::zeros(3 * N_LANDMARKS, 3 * N_LANDMARKS, CV_32FC1);
	vector<vector<vec3<float>>> stat3d;
	vector<vec3<float>> meanvec(N_LANDMARKS);

	vec3<float> zero;
	zero.x = 0.0;
	zero.y = 0.0;
	zero.z = 0.0;
	fill(meanvec.begin(), meanvec.begin() + N_LANDMARKS, zero);

	for (int i = 1; i <= N_MODELS; i++)
	{
		cout << i << endl;
		string stri;
		ostringstream temp;
		temp << i;
		stri = temp.str();


		ifstream alphafile;
		string alphafilename = "data/pts_stat/rhead" + stri + ".txt";

		alphafile.open(alphafilename);
		string str;
		//alpha
		getline(alphafile, str);
		//rotation
		getline(alphafile, str);
		//translation
		getline(alphafile, str);

		int n;
		vec2<float> point2d;
		int id_3d;
		vec3<float> point3d;
		vec3<float> mean3d;
		vec3<float> dist3d;

		vector<vec3<float>> points3d;

		while (alphafile >> n >> point2d.x >> point2d.y >> id_3d >>
			point3d.x >> point3d.y >> point3d.z >> mean3d.x >> mean3d.y >> mean3d.z >> dist3d.x >> dist3d.y >> dist3d.z)
		{
			points3d.push_back(point3d);
			meanvec[n - 1].x += point3d.x / N_MODELS;
			meanvec[n - 1].y += point3d.y / N_MODELS;
			meanvec[n - 1].z += point3d.z / N_MODELS;
		}

		stat3d.push_back(points3d);

		alphafile.close();
	}

	for (int mid = 0; mid < N_MODELS; mid++)
	{
		cout << mid << endl;

		for (int i = 0; i < N_LANDMARKS; i++)
		{
			Mat xi = Mat::zeros(3, 1, CV_32FC1);

			xi.at<float>(0, 0) = stat3d[mid][i].x - meanvec[i].x;
			xi.at<float>(1, 0) = stat3d[mid][i].y - meanvec[i].y;
			xi.at<float>(2, 0) = stat3d[mid][i].z - meanvec[i].z;

			//cout << xi.at<float>(0, 0) << " " << xi.at<float>(1, 0) << " " << xi.at<float>(2, 0) << endl;

			for (int j = 0; j < N_LANDMARKS; j++)
			{
				Mat component = Mat::zeros(3, 3, CV_32FC1);
				Mat xj = Mat::zeros(1, 3, CV_32FC1);

				xj.at<float>(0, 0) = stat3d[mid][j].x - meanvec[j].x;
				xj.at<float>(0, 1) = stat3d[mid][j].y - meanvec[j].y;
				xj.at<float>(0, 2) = stat3d[mid][j].z - meanvec[j].z;

				//cout << xj.at<float>(0, 0) << " " << xj.at<float>(0, 1) << " " << xj.at<float>(0, 2) << endl;

				component = xi * xj / N_MODELS; //n-1??
				 
				//for (int q = 0; q < 3; q++)
				//{
				//	for (int p = 0; p < 3; p++)
				//	{
				//		cout << component.at<float>(q, p) << " ";
				//	}
				//	cout << "\n";
				//}

				for (int q = 0; q < 3; q++)
				{
					for (int p = 0; p < 3; p++)
					{
						cov.at<float>(3 * i + q, 3 * j + p) += component.at<float>(q, p);
					}
				}

			}

		}

	}

	cout << cov.size();

	ofstream covfile;
	string covfilename = "data/cov3000.txt";
	covfile.open(covfilename);

	for (int i = 0; i < 3 * N_LANDMARKS; i++)
	{
		for (int j = 0; j < 3 * N_LANDMARKS; j++)
		{
			char pointoutput[100];
			sprintf(pointoutput, "%.12f ", cov.at<float>(i, j));
			covfile << pointoutput;
		}
		covfile << "\n";
	}

}

void draw_landmarks(cv::Mat image, vector<vec2<float>> landmarks, cv::Scalar color = cv::Scalar(255.0, 0.0, 0.0))
{
	auto num_landmarks = landmarks.size();
	for (int i = 0; i < num_landmarks; ++i) {
		cv::circle(image, cv::Point2f(landmarks[i].x, landmarks[i].y), 3, color, CV_FILLED, 16, 0);
	}
}

void drawNoiseDiff(Detector* detector, Render* glrender, int i)
{
	vector<vec2<float>> landmarks0;
	float angle = rand() % 60 - 30.f;
	float trans = rand() % 40000 - 20000.f;

	int width = 800;
	int height = 600;

	double gaussianNoiseLevel = 0.f;

	string stri;
	ostringstream temp;
	temp << i;
	stri = temp.str();

	string modelpath = basel_folder + modelname + stri + ".ply";
	string imagename = modelname + stri + ".jpg";
	string renderpath = exp_folder + render_folder + imagename;
	string landmarkspath = exp_folder + imagename;

	glrender->getScene(modelpath);
	glrender->coloredRender(true);
	glrender->setWindowSize(width, height);
	glrender->openWindow();
	glrender->randomCameraPose(angle, trans);
	glrender->renderScene();
	glrender->saveRender(renderpath);
	glrender->addGaussianNoise(renderpath, gaussianNoiseLevel);

	landmarks0 = detector->detectLandmarks(renderpath, landmarkspath);


	gaussianNoiseLevel = 5.f;

	glrender->renderScene();
	glrender->saveRender(renderpath);
	glrender->addGaussianNoise(renderpath, gaussianNoiseLevel);

	Mat image0 = cv::imread(renderpath);
	draw_landmarks(image0, landmarks0);
	cv::imwrite(renderpath, image0);

	vector<vec2<float>> landmarks = detector->detectLandmarks(renderpath, landmarkspath);

	Mat image = cv::imread(landmarkspath);
	for (int h = 0; h < landmarks0.size(); h++)
	{
		line(image, cv::Point2f(landmarks[h].x, landmarks[h].y), cv::Point2f(landmarks0[h].x, landmarks0[h].y), cv::Scalar(255.0, 255.0, 255.0),1);
	}
	cv::imwrite(landmarkspath, image);


	glrender->swapBuffers();
	glrender->releaseScene();

}

void getExperimentalData(Detector* detector, Render* glrender, int start, int end)
{
	/*
	Engine *ep;
	mxArray *T = NULL, *result = NULL;
	*/
	/*
	* Call engOpen with a NULL string. This starts a MATLAB process
	* on the current host using the command "matlab".
	*/
	/*
	if (!(ep = engOpen(""))) {
	fprintf(stderr, "\nCan't start MATLAB engine\n");
	return EXIT_FAILURE;
	}
	*/

	float trans = rand() % 40000 - 20000.f;

	for (int i = start; i <= end; i++)
	{
		int width = 800;
		int height = 600;
		
		//float angle = rand() % 60 - 30.f;
		//float trans = rand() % 40000 - 20000.f;
		
		float angle = -30.f + (i-1)*10.f;
		//float trans = 0.f;

		//double gaussianNoiseLevel = 5.0;
		double gaussianNoiseLevel = 0.f;


		string stri;
		ostringstream temp;
		temp << i;
		stri = temp.str();

		string modelpath = basel_folder + modelname + stri + ".ply";
		string imagename = modelname + stri + ".jpg";
		string renderpath = exp_folder + render_folder + imagename;
		string landmarkspath = exp_folder + imagename;

		glrender->getScene(modelpath);
		glrender->coloredRender(true);
		glrender->setWindowSize(width, height);
		glrender->randomCameraPose(angle, trans);
		glrender->openWindow();
		glrender->renderScene();
		glrender->saveRender(renderpath);
		glrender->addGaussianNoise(renderpath, gaussianNoiseLevel);


		vector<vec2<float>> landmarks = detector->detectLandmarks(renderpath, landmarkspath);

		//if (remove(renderpath.data()) != 0)
		//	perror("Error deleting file");

		/*
		vector<vec3<float>> vertices = glrender->vertices;

		for (int j = 0; j < landmarks.size(); j++)
		{
			vec3<float> point3d = glrender->get3DPoint(landmarks[j]);
			struct mvec p3d = find_closest(vertices, point3d);

			glrender->reverseTest(p3d.point, landmarksoutput);
		}
		*/
		vector<double> matR = glrender->getR();
		vector<double> vecT = glrender->getT();
		vector<double> vecF = glrender->getFocals();

		float fx = vecF[0] * width / 2.0;
		float fy = vecF[1] * height / 2.0;

		glrender->swapBuffers();
		glrender->releaseScene();

		string alphafilename = basel_folder + "alpha/" + modelname + stri + ".txt";
		string statfilename = exp_folder + modelname + stri + ".txt";

		printFormatedStat(alphafilename, statfilename,
						  landmarks, matR, vecT, width, height, fx, gaussianNoiseLevel);

	}

}

void renderPly(Detector* detector, Render* glrender, int i)
{
	/*
	Engine *ep;
	mxArray *T = NULL, *result = NULL;
	*/
	/*
	* Call engOpen with a NULL string. This starts a MATLAB process
	* on the current host using the command "matlab".
	*/
	/*
	if (!(ep = engOpen(""))) {
	fprintf(stderr, "\nCan't start MATLAB engine\n");
	return EXIT_FAILURE;
	}
	*/

	int width = 800;
	int height = 600;

	float angle = 0;
	float trans = rand() % 40000 - 20000.f;

	//float angle = rand() % 60 - 30.f;
	//float trans = rand() % 40000 - 20000.f;

	string stri;
	ostringstream temp;
	temp << i;
	stri = temp.str();

	string path = "data/rhead";

	string modelpath = path + stri + ".ply";
	string renderpath = path + stri + ".jpg";

	glrender->getScene(modelpath);
	glrender->coloredRender(true);
	glrender->randomCameraPose(angle, trans);
	glrender->openWindow();
	glrender->renderScene();
	glrender->saveRender(renderpath);

	glrender->swapBuffers();
	glrender->releaseScene();

	//angle = angle + rand() % 6 - 3.f;
	//trans = trans + rand() % 4000 - 2000.f;

	modelpath = path + stri + ".ply";
	renderpath = path + stri + "2.jpg";

	glrender->getScene(modelpath);
	glrender->coloredRender(false);
	glrender->setWindowSize(width, height);
	glrender->randomCameraPose(angle, trans);
	glrender->openWindow();
	glrender->renderScene();
	glrender->saveRender(renderpath);

	glrender->swapBuffers();
	glrender->releaseScene();

}

void getMeanData22pts(Detector* detector, Render* glrender)
{
	string modelpath = basel_folder + modelname + "0.ply";
	string imagename = modelname + "0.jpg";
	string renderpath = exp_folder + render_folder + imagename;
	string landmarkspath = exp_folder + mean_folder + imagename;
	string statfilename = exp_folder + mean_folder + modelname + "0.txt";

	glrender->getScene(modelpath);
	glrender->coloredRender(true);
	glrender->openWindow();
	glrender->renderScene();
	glrender->saveRender(renderpath);

	vector<vec2<float>> landmarks = detector->detectLandmarks(renderpath, landmarkspath);
	vector<vec3<float>> vertices = glrender->vertices;

	if (remove(renderpath.data()) != 0)
		perror("Error deleting file");

	ofstream statfile;
	statfile.open(statfilename);
	statfile << "points: " << landmarks.size() << endl;
	statfile << "{\n";

	for (int j = 0; j < landmarks.size(); j++)
	{
		vec3<float> point3d = glrender->get3DPoint(landmarks[j]);
		struct mvec p3d = find_closest(vertices, point3d);

		glrender->reverseTest(p3d.point, landmarkspath);

		char pointoutput[300];
		sprintf(pointoutput, "%.2f %.2f %d\n", landmarks[j].x, landmarks[j].y, p3d.num);
		statfile << pointoutput;
	}

	statfile << "}\n";
	statfile.close();

	glrender->swapBuffers();
	glrender->releaseScene();

}

int _tmain(int argc, char *argv[])
{
	//getModelStat();
	//calcCovMat(3000, 22);

	
	Detector* detector = new Detector();
	Render* glrender = new Render(800, 600, 45.0);

	//getMeanData22pts(detector, glrender);

	getExperimentalData(detector, glrender, 1, 7);
	//drawNoiseDiff(detector, glrender, 1);
	
	//for (int i = 24; i <= 24; i++) renderPly(detector, glrender, i);
	

	delete glrender;
	delete detector;
	
	
	cout << "----finished successfully!----" << endl;
	
	return 0;
	
}

