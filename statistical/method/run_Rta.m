clc;
addpath(genpath('extras'))
addpath helpers

%%%%% Init %%%%%
N_MODELS = 50;
N_METH = 13;
exp_errors  = zeros(N_METH, N_MODELS);

ROTATION_LSQ_SCALE    = 1e2;
TRANSLATION_LSQ_SCALE = 1e8;
ALPHA_LSQ_SCALE       = 1e4;
OPNP_SCALE            = 1e4;

PATH_TO_MODEL     = './bfm/01_MorphableModel.mat';
PATH_TO_EXPDATA   = './data22/alpha199/rhead';
PATH_TO_EXP_OUT   = './data22/stat/Rta.txt';
PATH_TO_MEAN_PTS  = './data22/mean/rhead0.txt'; 
PATH_TO_COV       = './data22/stat/cov2.txt';
  
% read detected points on mean model
[~, face0_ids] = read_mean_pts(PATH_TO_MEAN_PTS);
% face0_id start with 0
n_points  = size(face0_ids, 2);
    
model = load(PATH_TO_MODEL);
shape_dim  = size(model.shapePC, 2);
n_tex_dim  = size(model.texPC,   2);
tex_coeff  = zeros(n_tex_dim , 1);

alpha0 = zeros(shape_dim, 1);
face0 = coef2object(alpha0, model.shapeMU, model.shapePC, model.shapeEV);
sigma = model.shapeEV;
  
mean_3dpts = zeros(3, n_points);
for k = 1:n_points
    mean_3dpts(:, k) = face0((3*face0_ids(k) + 1):(3*face0_ids(k) + 3));
end

Eid  = eye(3*n_points, 3*n_points);
Enid = dlmread(PATH_TO_COV);

[logid, ~] = fopen(PATH_TO_EXP_OUT, 'w');
methods_string = 'OPnP R  |  OPnP t   | OPnP3D R | OPnP3D t | NonIdCov R | NonIdCov t | NonIdCov A       | IdCov R | IdCov t   | IdCov A             | Fixed R  | Fixed t   | Fixed A\n';
fprintf(logid,methods_string);

for eid=1:N_MODELS
    disp(strcat('Calculating-', num2str(eid),'-model...'));
    %%%%% Read experiment info %%%%%
    expfilename = strcat(PATH_TO_EXPDATA, num2str(eid),'.txt');
    [WIDTH, HEIGHT, ~, FOCAL_LENGTH, LANDMARKS, ALPHA, TRUTH_R, TRUTH_T] = get_exp_param(expfilename);
        
    Kmat = [FOCAL_LENGTH 0.0           WIDTH/2; 
            0.0          FOCAL_LENGTH  HEIGHT/2; 
            0.0          0.0           1.0];
    
    Kinv = inv(Kmat);
    
    
    %%%%% INIT RECONSTRUCTION PARAMS %%%%%
    
    params.TRANSLATION_LSQ_SCALE = TRANSLATION_LSQ_SCALE;
    params.ROTATION_LSQ_SCALE = ROTATION_LSQ_SCALE;
    params.OPNP_SCALE = OPNP_SCALE;
    params.ALPHA_LSQ_SCALE = ALPHA_LSQ_SCALE ;
    params.model = model;
    params.shape_dim = shape_dim;    
    params.ptsN = n_points;
    params.mean_points = mean_3dpts;
    params.mean_id = face0_ids;
    params.Kinv = Kinv;
    params.landmarks = LANDMARKS;

    % Use OPNP to find R, t
    proj_landmarks = Kinv * [LANDMARKS; ones(1, n_points)];
    [R0, t0] = OPnP(mean_3dpts/OPNP_SCALE, proj_landmarks);
    t0 = t0*OPNP_SCALE;
    
    er_opnp = cal_pose_err([R0 t0],[TRUTH_R TRUTH_T]);
    %disp(er_opnp(1));
    %disp(er_opnp(2)); 
    exp_errors(1, eid) = er_opnp(1);
    exp_errors(2, eid) = er_opnp(2);
    
    % Use OPNP3DFULL to find R, t
    [R0f t0f error0 flag] = OPnP3DFull(mean_3dpts/OPNP_SCALE,proj_landmarks(1:2,:),Enid);
    t0f = t0f*OPNP_SCALE;
    
    er_opnp3Dfull = cal_pose_err([R0f t0f],[TRUTH_R TRUTH_T]);
    %disp(er_opnp3Dfull(1));
    %disp(er_opnp3Dfull(2));
    exp_errors(3, eid) = er_opnp3Dfull(1);
    exp_errors(4, eid) = er_opnp3Dfull(2);
    
    
    %%%%% START NONID COV MAT RECONSTRUCTION %%%%%    
    paramsCovNonId = params;
    paramsCovNonId.Emat = Enid;
    [Rnid Tnid Anid] = Rta_OI(paramsCovNonId);
    
    er_CovNonIdRt = cal_pose_err([Rnid Tnid],[TRUTH_R TRUTH_T]);
    er_CovNonIdA = cal_alpha_err(sigma,ALPHA,Anid);
    %disp(er_CovNonIdRt(1));
    %disp(er_CovNonIdRt(2));
    %disp(er_CovNonIdA);
    %disp(sum(abs(ALPHA-Anid)));
    %disp(ALPHA);
    %disp(Anid);
    
    exp_errors(5, eid) = er_CovNonIdRt(1);
    exp_errors(6, eid) = er_CovNonIdRt(2);
    exp_errors(7, eid) = er_CovNonIdA;
    
    %shape = coef2object( Anid, model.shapeMU, model.shapePC, model.shapeEV );
    %tex   = coef2object( tex_coeff, model.texMU,   model.texPC,   model.texEV );
    %filename = strcat('./data22/rhead', num2str(eid),'.ply');
    %plywrite(filename, shape, tex, model.tl );
    
    
    %%%%% START ID COV MAT RECONSTRUCTION %%%%%
    paramsCovId = params;
    paramsCovId.Emat = Eid;
    [Rid Tid Aid] = Rta_OI(paramsCovId);
    
    er_CovIdRt = cal_pose_err([Rid Tid],[TRUTH_R TRUTH_T]);
    er_CovIdA = cal_alpha_err(sigma,ALPHA,Aid);
    
    exp_errors(8, eid) = er_CovIdRt(1);
    exp_errors(9, eid) = er_CovIdRt(2);
    exp_errors(10, eid) = er_CovIdA;
    
    
    %%%%% START FIXED RECONSTRUCTION %%%%%    
    paramsFixed = params;
    paramsFixed.TRANSLATION_LSQ_SCALE = 1e8;
    paramsFixed.ROTATION_LSQ_SCALE = 1e2;
    paramsFixed.ALPHA_LSQ_SCALE = 1e9;
    paramsFixed.REG_SCALE = 1e-7;
    
    display(paramsFixed.ALPHA_LSQ_SCALE);
    display(paramsFixed.REG_SCALE);
    
    [Rfix Tfix Afix] = Rta_fixed(paramsFixed);
    er_fixRt = cal_pose_err([Rfix Tfix],[TRUTH_R TRUTH_T]);
    er_fixA = cal_alpha_err(sigma,ALPHA,Afix);
    %disp(er_fixRt(1));
    %disp(er_fixRt(2));
    %disp(er_fixA);
    %disp(sum(abs(ALPHA-Afix)));
    
    exp_errors(11, eid) = er_fixRt(1);
    exp_errors(12, eid) = er_fixRt(2);
    exp_errors(13, eid) = er_fixA;
   
 
    for pid=1:N_METH
        fprintf(logid,'%3.6f   ', exp_errors(pid, eid));
    end 
    fprintf(logid,'\n');
    
end
fprintf(logid,'------\n');
fprintf(logid,'MEAN\n');
for i=1:N_METH
    fprintf(logid,'%3.6f   ', mean(exp_errors(i, :)));   
end
fprintf(logid,'\n');
fprintf(logid,'MEDIAN\n');
for i=1:N_METH
    fprintf(logid,'%3.6f   ', median(exp_errors(i, :)));   
end
fprintf(logid,'\n');
fclose(logid);
