clc;
addpath(genpath('extras'))
addpath helpers

%%%%% Init %%%%%
%N_MODELS = 50;
N_MODELS = 100;
N_METH = 2+2+2+2+2;
exp_errors  = zeros(N_METH, N_MODELS);

OPNP_SCALE            = 1e4;

PATH_TO_MODEL     = './bfm/01_MorphableModel.mat';
PATH_TO_EXPDATA   = './data22/rhead';
PATH_TO_EXP_OUT   = './data22/stat/new_Rt_OPnP3D_cov.txt';
PATH_TO_MEAN_PTS  = './data22/mean/rhead0.txt'; 
  
% read detected points on mean model
[~, face0_ids] = read_mean_pts(PATH_TO_MEAN_PTS);
% face0_id start with 0
n_points  = size(face0_ids, 2);
    
model = load(PATH_TO_MODEL);
shape_dim  = size(model.shapePC, 2);

alpha0 = zeros(shape_dim, 1);
face0 = coef2object(alpha0, model.shapeMU, model.shapePC, model.shapeEV);
sigma = model.shapeEV;
  
mean_3dpts = zeros(3, n_points);
for k = 1:n_points
    mean_3dpts(:, k) = face0((3*face0_ids(k) + 1):(3*face0_ids(k) + 3));
end

Eid  = eye(3*n_points, 3*n_points);
Enid2 = dlmread('./data22/stat/cov2.txt');
Enid4 = dlmread('./data22/stat/cov4.txt');
Enid6 = dlmread('./data22/stat/cov6.txt');
Enid1000 = dlmread('./data22/stat/cov1000.txt');

[logid, ~] = fopen(PATH_TO_EXP_OUT, 'w');
methods_string = 'OPnP R  |  OPnP t   |SmallCov OPnP3D R|SmallCov OPnP3D t|MediumCov OPnP3D R|MediumCov OPnP3D t|BigCov OPnP3D R|BigCov OPnP3D t|AllCov OPnP3D R|AllCov OPnP3D t \n';
fprintf(logid,methods_string);

for eid=1:N_MODELS
    disp(strcat('Calculating-', num2str(eid),'-model...'));
    %%%%% Read experiment info %%%%%
    expfilename = strcat(PATH_TO_EXPDATA, num2str(eid),'.txt');
    [WIDTH, HEIGHT, ~, FOCAL_LENGTH, LANDMARKS, ALPHA, TRUTH_R, TRUTH_T] = get_exp_param(expfilename);
        
    Kmat = [FOCAL_LENGTH 0.0           WIDTH/2; 
            0.0          FOCAL_LENGTH  HEIGHT/2; 
            0.0          0.0           1.0];
    
    Kinv = inv(Kmat);

    % Use OPNP to find R, t
    proj_landmarks = Kinv * [LANDMARKS; ones(1, n_points)];
    [R0, t0] = OPnP(mean_3dpts/OPNP_SCALE, proj_landmarks);
    t0 = t0*OPNP_SCALE;
    
    er_opnp = cal_pose_err([R0 t0],[TRUTH_R TRUTH_T]);
    exp_errors(1, eid) = er_opnp(1);
    exp_errors(2, eid) = er_opnp(2);
    
    % Use OPNP3DFULL to find R, t
    [R0f t0f error0 flag] = OPnP3DFull(mean_3dpts/OPNP_SCALE,proj_landmarks(1:2,:),Enid2);
    t0f = t0f*OPNP_SCALE;
    
    er_opnp3Dfull = cal_pose_err([R0f t0f],[TRUTH_R TRUTH_T]);
    exp_errors(3, eid) = er_opnp3Dfull(1);
    exp_errors(4, eid) = er_opnp3Dfull(2);
    
    
    [R0f t0f error0 flag] = OPnP3DFull(mean_3dpts/OPNP_SCALE,proj_landmarks(1:2,:),Enid4);
    t0f = t0f*OPNP_SCALE;
    er_opnp3Dfull = cal_pose_err([R0f t0f],[TRUTH_R TRUTH_T]);
    exp_errors(5, eid) = er_opnp3Dfull(1);
    exp_errors(6, eid) = er_opnp3Dfull(2);
    
    
    [R0f t0f error0 flag] = OPnP3DFull(mean_3dpts/OPNP_SCALE,proj_landmarks(1:2,:),Enid6);
    t0f = t0f*OPNP_SCALE;
    er_opnp3Dfull = cal_pose_err([R0f t0f],[TRUTH_R TRUTH_T]);
    exp_errors(7, eid) = er_opnp3Dfull(1);
    exp_errors(8, eid) = er_opnp3Dfull(2);

    
    [R0f t0f error0 flag] = OPnP3DFull(mean_3dpts/OPNP_SCALE,proj_landmarks(1:2,:),Enid1000);
    t0f = t0f*OPNP_SCALE;
    er_opnp3Dfull = cal_pose_err([R0f t0f],[TRUTH_R TRUTH_T]);
    exp_errors(9, eid) = er_opnp3Dfull(1);
    exp_errors(10, eid) = er_opnp3Dfull(2);    
    
    for pid=1:N_METH
        fprintf(logid,'%3.6f   ', exp_errors(pid, eid));
    end 
    fprintf(logid,'\n');
    
end
fprintf(logid,'------\n');
fprintf(logid,'MEAN\n');
for i=1:N_METH
    fprintf(logid,'%3.6f   ', mean(exp_errors(i, :)));   
end
fprintf(logid,'\n');
fprintf(logid,methods_string);
fprintf(logid,'MEDIAN\n');
for i=1:N_METH
    fprintf(logid,'%3.6f   ', median(exp_errors(i, :)));   
end
fprintf(logid,'\n');
fclose(logid);
