% OPnP OPnP3D OPnP+Proj OPnP+OI Proposed 
% R mean (R t)
%data = [
%1.563350 0; 1.522320 0; 1.486783 0; 0.760291 0;
%];
%createRbar(data')

% R median (R t)
%data = [
%1.321530 0; 1.222320 0; 1.223988 0; 0.598160 0;
%];
%createRbar(data')

%T mean (R t)
data = [
3.495970 0; 3.431226 0; 3.417070 0; 1.432842 0;
];
createRbar(data')

% T median (R t)
%data = [
%2.883051 0; 2.2912342 0; 2.821430 0; 1.184342 0;
%];
%createRbar(data')



% OPnP OPnP3D OPnP+Proj OPnP+OI Proposed 
% R mean (R t alpha)
%data = [
%3.383689 0; 3.557299 0; 3.401896 0; 3.296816 0;
%];
%createRbar(data')

% R median (R t alpha)
%data = [
%2.965790 0; 3.200749 0; 3.026433  0; 2.895718 0;
%];
%createRbar(data')

% T mean (R t alpha)
%data = [
%3.881500 0; 4.054798 0; 3.923987 0; 3.806770 0;
%];
%createRbar(data')

% T median (R t alpha)
%data = [
%3.813954 0; 3.932538 0; 4.031845 0; 3.741732 0;
%];
%createRbar(data')



% OPnP+Proj OPnP+OI Proposed
% Alpha mean (R t alpha)
%data = [
%1461775098183.679900 0; 1143392419512.320100 0; 765447947223.040040 0; 
%];
%createAbar(data')

% Alpha median (R t alpha)
%data = [
%1053864329216.000000 0; 990969069568.000000 0; 582294044672.000000 0; 
%];
%createAbar(data')




% small medium big all
% R cov mean (R t)
%data = [
%0.760291 0; 1.163682 0; 0.978573 0; 0.842841 0; 
%];
%createCovbar(data')

% R cov median (R t)
%data = [
%0.598160 0; 0.829588 0; 0.716539 0; 1.043645 0; 
%];
%createCovbar(data')

% T cov mean (R t)
%data = [
%1.432842 0; 1.624353 0; 1.683431 0; 2.022319 0; 
%];
%createCovbar(data')

% T cov median (R t)
%data = [
%1.184342 0; 1.674361 0; 1.242347 0; 1.942327 0; 
%];
%createCovbar(data')




% small medium big all
% R cov mean (R t a)
%data = [
%3.296816 0; 3.374242 0; 3.421351 0; 3.325738 0; 
%];
%createCovbar(data')

% R cov median (R t a)
%data = [
%2.895718 0; 3.124241 0; 3.092142 0; 2.997764 0; 
%];
%createCovbar(data')

% T cov mean (R t a)
%data = [
%3.806770 0; 3.886421 0; 3.942315 0; 3.868371 0; 
%];
%createCovbar(data')

% T cov median (R t a)
%data = [
%3.741732 0; 3.823178 0; 3.770921 0; 3.9041247 0; 
%];
%createCovbar(data')

% Alpha cov mean (R t a)
%data = [
%765447947223.040040 0; 957482815446.000000 0; 894167765331.000000 0; 798752486001.000000 0; 
%];
%createCovbar(data')

% Alpha cov median (R t a)
%data = [
%582294044672.000000 0;  756824560025.000000 0; 698211154375.000000 0; 720675148726.000000 0; 
%];
%createCovbar(data')


