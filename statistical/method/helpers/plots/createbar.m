function createbar(ymatrix1)
%CREATEFIGURE(YMATRIX1)
%  YMATRIX1:  bar matrix data

%  Auto-generated by MATLAB on 09-May-2016 18:00:30

% Create figure
figure1 = figure('InvertHardcopy','off','Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1,'XTickLabel',{'',''},'XTick',[1 2]);
% Uncomment the following line to preserve the X-limits of the axes
% xlim(axes1,[0.5 2.5]);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0 6]);
% Uncomment the following line to preserve the Z-limits of the axes
% zlim(axes1,[-1 1]);
box(axes1,'on');
hold(axes1,'all');

% Create multiple lines using matrix input to bar
bar1 = bar(ymatrix1,'Parent',axes1);
set(bar1(1),...
    'FaceColor',[63/255 81/255 181/255],...
    'DisplayName','OPnP');
%set(bar1(2),...
%    'FaceColor',[156/255 39/255 176/255],...
%    'DisplayName','OPnP3D');
set(bar1(2),...
    'FaceColor',[0.91372549533844 0.117647059261799 0.388235300779343],...
    'DisplayName','OPnP+Proj');
set(bar1(3),...
    'FaceColor',[244/255 67/255 54/255],...
    'DisplayName','OPnP+OI');
set(bar1(4),...
    'FaceColor',[67/255 160/255 71/255],...
    'DisplayName','Proposed');

% Create ylabel
ylabel({'Mean error'},'FontSize',12);

% Create xlabel
xlabel({'Methods'},'FontSize',12);

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.516307893020222 0.499057829759584 0.334637964774951 0.358024691358025]);

