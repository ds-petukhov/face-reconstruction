function [ error ] = cal_alpha_err( sigma, a_thuth, a_real )
%CALC_ALPHA_ERROR Summary of this function goes here
%   Detailed explanation goes here
    error = 0;
    N = length(sigma);
    for i=1:N
        error = error + (sigma(i)*(a_thuth(i) - a_real(i)))^2;
    end
end

