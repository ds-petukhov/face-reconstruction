function [ WIDTH, HEIGHT, NOISE_LEVEL, FOCAL_LENGTH, LANDMARKS, TRUTH_ALPHA, TRUTH_R, TRUTH_T ] = get_exp_param( expfilename )
%GET_EXP_PARAM Summary of this function goes here
%   Detailed explanation goes here
    [exp_fid, ~] = fopen(expfilename, 'r');
    
    WIDTH = 800;
    HEIGHT = 600;
    FOCAL_LENGTH = 1.792591;
    LANDMARKS_NUM = 0;
    ALPHA_DIM = 0;
    TRUTH_R = eye(3,3);
    TRUTH_T = ones(3,1);
    
    line = fgets(exp_fid);
    if ~isempty(strfind(line, 'resolution'))
        [cells, ~] = textscan(exp_fid,'%f %f');
        [WIDTH, HEIGHT] = cells{:};
    end
        
    NOISE_LEVEL = 0.0;
    line = fgets(exp_fid);
    if ~isempty(strfind(line, 'noise level'))
        [cells, ~] = textscan(exp_fid,'%f');
        [NOISE_LEVEL] = cells{:};
    end
    
    line = fgets(exp_fid);
    if ~isempty(strfind(line, 'focal length'))
        [cells, ~] = textscan(exp_fid,'%f');
        [FOCAL_LENGTH] = cells{:};
    end
    
    line = fgets(exp_fid);
    if ~isempty(strfind(line, 'landmarks count'))
        [cells, ~] = textscan(exp_fid,'%d');
        [LANDMARKS_NUM] = cells{:};
        
        line = fgets(exp_fid);
        if ~isempty(strfind(line, 'landmarks'))
            landmarksdata = textscan(exp_fid,'%f %f',LANDMARKS_NUM,'Delimiter','\n');
            [ptsx, ptsy] = landmarksdata{:};
        end
        LANDMARKS = [ptsx, ptsy]';
    end
    
    line = fgets(exp_fid);
    if ~isempty(strfind(line, 'alpha dim'))
        [cells, ~] = textscan(exp_fid,'%d');
        [ALPHA_DIM] = cells{:};
        
        line = fgets(exp_fid);
        if ~isempty(strfind(line, 'truth alpha'))
            landmarksdata = textscan(exp_fid,'%f',ALPHA_DIM,'Delimiter','\n');
            [valpha] = landmarksdata{:};
        end
        TRUTH_ALPHA = valpha;
    end
    
    line = fgets(exp_fid);
    if ~isempty(strfind(line, 'truth rotation'))
        [cells] = textscan(exp_fid,'%f',9,'Delimiter','\n');
        TRUTH_R = reshape(cells{:},3,3,[]);
    end
    
    line = fgets(exp_fid);
    if ~isempty(strfind(line, 'truth translation'))
        [cells] = textscan(exp_fid,'%f',3,'Delimiter','\n');
        TRUTH_T = cells{:};
    end
    
    fclose(exp_fid);
end

