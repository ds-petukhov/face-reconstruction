function [ points, ids ] = read_mean_pts( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    [fid, message] = fopen(filename, 'r');
    line = fgets(fid);
    if ~isempty(strfind(line, 'points'))
        filedata = textscan(fid, '%f %f %d', 'delimiter', ' ', 'HeaderLines', 1);
        [x, y, z] = filedata{:};
    end
    points = [x, y]';
    ids = z';
    fclose(fid);

end

