function [ mat ] = covmat_to_cam( covmat, Rcam )
%COVMAT_TO_CAM Summary of this function goes here
%   Detailed explanation goes here

    [m n] = size(covmat);
    mat = eye(m, n); 
    % Cov matrix to camera coordinates
    for col = 1:n/3
        for row = 1:m/3
            cov_mat_component = covmat(3*row - 2:3*row, 3*col - 2:3*col);
            mat(3*row - 2:3*row, 3*col - 2:3*col) = Rcam * cov_mat_component * Rcam';
        end
    end
    
end

