clc;
addpath(genpath('extras'))
addpath helpers

%%%%% Init %%%%%
N_MODELS = 7;
N_METH = 10;
exp_errors  = zeros(N_METH, N_MODELS);

ROTATION_LSQ_SCALE    = 1e4;
TRANSLATION_LSQ_SCALE = 1e8;
OPNP_SCALE            = 1e4;

PATH_TO_MODEL     = './bfm/01_MorphableModel.mat';
%PATH_TO_EXPDATA   = './data22/rotation/alpha1/rhead';
PATH_TO_EXPDATA   = './data22/noise/rt/rhead';
PATH_TO_EXP_OUT   = './data22/stat/Rt_noise.txt';
PATH_TO_MEAN_PTS  = './data22/mean/rhead0.txt'; 
PATH_TO_COV       = './data22/stat/cov2.txt';
  
% read detected points on mean model
[~, face0_ids] = read_mean_pts(PATH_TO_MEAN_PTS);
% face0_id start with 0
n_points  = size(face0_ids, 2);
    
model = load(PATH_TO_MODEL);
shape_dim  = size(model.shapePC, 2);
alpha0 = zeros(shape_dim, 1);
face0 = coef2object(alpha0, model.shapeMU, model.shapePC, model.shapeEV);
  
mean_3dpts = zeros(3, n_points);
for k = 1:n_points
    mean_3dpts(:, k) = face0((3*face0_ids(k) + 1):(3*face0_ids(k) + 3));
end

Eid  = eye(3*n_points, 3*n_points);
Enid = dlmread(PATH_TO_COV);

[logid, ~] = fopen(PATH_TO_EXP_OUT, 'w');
methods_string = 'OPnP R  |  OPnP t   | OPnP3D R | OPnP3D t | CovId R  | CovId t  | NonIdCov R | NonIdCov t | Fixed R | Fixed t \n';
fprintf(logid,methods_string);

for eid=1:N_MODELS
    %%%%% Read experiment info %%%%%
    disp(strcat('Calculating-', num2str(eid),'-model...'));
    expfilename = strcat(PATH_TO_EXPDATA, num2str(eid),'.txt');
    [WIDTH, HEIGHT, ~, FOCAL_LENGTH, LANDMARKS, ALPHA, TRUTH_R, TRUTH_T] = get_exp_param(expfilename);
        
    Kmat = [FOCAL_LENGTH 0.0           WIDTH/2; 
            0.0          FOCAL_LENGTH  HEIGHT/2; 
            0.0          0.0           1.0];
    
    Kinv = inv(Kmat);

    %%%%% INIT RECONSTRUCTION PARAMS %%%%%
    params.TRANSLATION_LSQ_SCALE = TRANSLATION_LSQ_SCALE;
    params.ROTATION_LSQ_SCALE = ROTATION_LSQ_SCALE;
    params.OPNP_SCALE = OPNP_SCALE;
    params.model = model;
    params.shape_dim = shape_dim;  
    params.ptsN = n_points;
    params.mean_points = mean_3dpts;
    params.mean_id = face0_ids;
    params.Kinv = Kinv;
    params.landmarks = LANDMARKS;
    
    % Use OPNP to find R, t
    proj_landmarks = Kinv * [LANDMARKS; ones(1, n_points)];
    [R0, t0] = OPnP(mean_3dpts/OPNP_SCALE, proj_landmarks);
    t0 = t0*OPNP_SCALE;  
    er_opnp = cal_pose_err([R0 t0],[TRUTH_R TRUTH_T]);
    %disp(er_opnp(1));
    %disp(er_opnp(2));
    exp_errors(1, eid) = er_opnp(1);
    exp_errors(2, eid) = er_opnp(2);
    
    % Use OPNP3DFULL to find R, t
    [R0f t0f error0 flag] = OPnP3DFull(mean_3dpts/OPNP_SCALE,proj_landmarks(1:2,:),Enid);
    t0f = t0f*OPNP_SCALE;
    er_opnp3Dfull = cal_pose_err([R0f t0f],[TRUTH_R TRUTH_T]);
    %disp(er_opnp3Dfull(1));
    %disp(er_opnp3Dfull(2)); 
    exp_errors(3, eid) = er_opnp3Dfull(1);
    exp_errors(4, eid) = er_opnp3Dfull(2); 
    
       
    %%%%% START ID COV MAT RECONSTRUCTION %%%%%    
    paramsCovId = params;
    paramsCovId.Emat = Eid;
    %paramsCovId.ROTATION_LSQ_SCALE = 1e4;
    %paramsCovId.TRANSLATION_LSQ_SCALE = 1e8;
    [Rid Tid] = Rt_OI(paramsCovId);
    er_id = cal_pose_err([Rid Tid],[TRUTH_R TRUTH_T]);
    exp_errors(5, eid) = er_id(1);
    exp_errors(6, eid) = er_id(2);
    
    %%%%% START NONID COV MAT RECONSTRUCTION %%%%%
    paramsCovNonId = params;
    paramsCovNonId.Emat = Enid;
    %paramsCovNonId.ROTATION_LSQ_SCALE = 1e4;
    %paramsCovNonId.TRANSLATION_LSQ_SCALE = 1e8;
    [Rnid Tnid] = Rt_OI(paramsCovNonId);
    er_nid = cal_pose_err([Rnid Tnid],[TRUTH_R TRUTH_T]);
    exp_errors(7, eid) = er_nid(1);
    exp_errors(8, eid) = er_nid(2);  
    
    %%%%% START FIXED %%%%%  
    paramsFixed = params;
    [Rfix Tfix] = Rt_fixed(paramsFixed);
    er_fix = cal_pose_err([Rfix Tfix],[TRUTH_R TRUTH_T]);
    exp_errors(9, eid) = er_fix(1);
    exp_errors(10, eid) = er_fix(2);  

    %calc RMSE
    %Dif = abs(Rrec - TRUTH_R).^2;
    %rec_RMSE = sqrt(sum(Dif(:))/numel(TRUTH_R));
    %display(rec_RMSE); 
    
    for pid=1:N_METH
        fprintf(logid,'%3.6f   ', exp_errors(pid, eid));
    end 
    fprintf(logid,'\n');
    
end
fprintf(logid,'------\n');
fprintf(logid,methods_string);
fprintf(logid,'MEAN\n');
for i=1:N_METH
    fprintf(logid,'%3.6f   ', mean(exp_errors(i, :)));   
end
fprintf(logid,'\n');

fprintf(logid,'MEDIAN\n');
for i=1:N_METH
    fprintf(logid,'%3.6f   ', median(exp_errors(i, :)));   
end
fprintf(logid,'\n');
fclose(logid);
