function [ R t ] = Rt_OI( params )
%RECONSTRUCTION Summary of this function goes here
%   Detailed explanation goes here
    OPNP_SCALE = params.OPNP_SCALE;
    TRANSLATION_LSQ_SCALE = params.TRANSLATION_LSQ_SCALE;
    ROTATION_LSQ_SCALE = params.ROTATION_LSQ_SCALE;
    N = params.ptsN;
    LANDMARKS = params.landmarks;
    model = params.model;
    shape_dim = params.shape_dim;
    mean_id = params.mean_id;
    mean_points = params.mean_points;
    Kinv = params.Kinv;
    
    proj_landmarks = Kinv * [LANDMARKS; ones(1, N)];
    % Use oPNP to find R, t
    [R0, t0] = OPnP(mean_points / OPNP_SCALE, proj_landmarks);
    t0 = t0 * OPNP_SCALE;

    Ecam = covmat_to_cam(params.Emat, R0);
    
    rot0 = rodrigues(R0) / ROTATION_LSQ_SCALE;    
    % init point for lsq
    x0 = [rot0', t0' / TRANSLATION_LSQ_SCALE];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%% Call lsqnonlin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    problem.x0        = x0;
    problem.objective = @calc_energy;
    problem.options   = optimset( ...
                                  'Algorithm',        'levenberg-marquardt' ...
                                , 'Jacobian',         'off'                 ...
                                , 'Display',          'off'                ...
                                , 'TolFun',            1e-15                ...
                                , 'TolX',              1e-15                ...
                                , 'MaxIter',           20                   ...
                        );
    problem.solver    = 'lsqnonlin';
    result            = lsqnonlin(problem);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%% Compute results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    rot  = result(1 : 3)';
    R    = rodrigues(rot * ROTATION_LSQ_SCALE);
    t    = result(4 : 6)' * TRANSLATION_LSQ_SCALE;    


    %%%%%%%%%%%%%%%%%%%%%%%%%%% Energy function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function energy = calc_energy(point)
        R1 = rodrigues(point(1 : 3)' * ROTATION_LSQ_SCALE);
        t1 = point(4 : 6)' * TRANSLATION_LSQ_SCALE;
        
        Xcam = zeros(3*N, 1); % model point in camera coordinates
        M    = zeros(3*N, 3*N);
        
        alpha = zeros(shape_dim, 1);
        face  = coef2object(alpha, model.shapeMU, model.shapePC, model.shapeEV);
        
        for i = 1:N
            point_model = face((3 * mean_id(i) + 1):(3 * mean_id(i) + 3)); %/ 1e4
            point_cam = R1 * point_model + t1;
            Xcam(3*i-2 : 3*i, 1) = point_cam;
            
            Xh = Kinv * [LANDMARKS(:, i); 1];
            Xn = Xh / norm(Xh);
            L  = Xn * Xn';
            M(3*i - 2:3*i , 3*i -2:3*i) = eye(3, 3) - L;
            
            %tempy = (eye(3, 3) - L) * point_cam;
        end
        %3d point error
        e = M * Xcam;
        
        %get sqrt from pseudoinverse matrix
        A = M * Ecam * M';
        Eproj = pinv(A);
        [EU,ES,EV] = svd(Eproj);
        for i = 1:size(ES, 1)
            if (abs(ES(i,i)) > 0)
                ES(i,i) = sqrt(ES(i,i));
            end
        end
        Esqrt = ES*EV';
        
        energy = Esqrt * e; %/ 1e4
    end
end

