function [ R t a] = Rta_fixed( params )
%RECONSTRUCTION_ALPHA Summary of this function goes here
%   Detailed explanation goes here
    OPNP_SCALE = params.OPNP_SCALE;
    ALPHA_LSQ_SCALE = params.ALPHA_LSQ_SCALE;
    TRANSLATION_LSQ_SCALE = params.TRANSLATION_LSQ_SCALE;
    ROTATION_LSQ_SCALE = params.ROTATION_LSQ_SCALE;
    REG_SCALE = params.REG_SCALE;
    N = params.ptsN;
    LANDMARKS = params.landmarks;
    model = params.model;
    shape_dim = params.shape_dim;
    mean_id = params.mean_id;
    mean_points = params.mean_points;
    Kinv = params.Kinv;
    
    proj_landmarks = Kinv * [LANDMARKS; ones(1, N)];
    % Use oPNP to find R, t
    [R0, t0] = OPnP(mean_points / OPNP_SCALE, proj_landmarks);
    t0 = t0 * OPNP_SCALE;
    
    rot0 = rodrigues(R0) / ROTATION_LSQ_SCALE;  
    a0 = zeros(shape_dim, 1);

    % init point for lsq
    x0 = [rot0', t0' / TRANSLATION_LSQ_SCALE, a0' / ALPHA_LSQ_SCALE];

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%% Call lsqnonlin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    problem.x0        = x0;
    problem.objective = @calc_energy;
    problem.options   = optimset( ...
                                  'Algorithm',        'levenberg-marquardt' ...
                                , 'Jacobian',         'off'                 ...
                                , 'Display',          'iter'                ...
                                , 'TolFun',            1e-20                ...
                                , 'TolX',              1e-20                ...
                                , 'MaxIter',           20                   ...
                        );
    problem.solver    = 'lsqnonlin';
    result            = lsqnonlin(problem);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%% Compute results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [rm rn] = size(result);
    rot     = result(1 : 3)';
    R       = rodrigues(rot * ROTATION_LSQ_SCALE);
    t       = result(4 : 6)' * TRANSLATION_LSQ_SCALE;    
    a       = result(7 : rn)' * ALPHA_LSQ_SCALE;

    %%%%%%%%%%%%%%%%%%%%%%%%%%% Energy function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function energy = calc_energy(point)
        [pm pn] = size(point);
        energy = zeros(1, 2*N + shape_dim);
        
        R1 = rodrigues(point(1 : 3)' * ROTATION_LSQ_SCALE);
        t1 = point(4 : 6)' * TRANSLATION_LSQ_SCALE;
        a1 = point(7 : pn)' * ALPHA_LSQ_SCALE;
        
        face  = coef2object(a1, model.shapeMU, model.shapePC, model.shapeEV);
        
        for i = 1:N
            point_model = face((3 * mean_id(i) + 1):(3 * mean_id(i) + 3)); %/ 1e4
            proj = inv(Kinv)*(R1 * point_model + t1);
                        
            proj = proj ./ proj(3);
            
            energy(1, 2*i - 1) = proj(1) - LANDMARKS(1, i);
            energy(1, 2*i    ) = proj(2) - LANDMARKS(2, i);
        end
        
        sigma = model.shapeEV;
        
        for j = 1:shape_dim
            energy(1, N + j) = sigma(j)*a1(j) * REG_SCALE;
        end
    end
end

