#!/usr/bin/python
import numpy as np
import multiprocessing
from scipy import misc, ndimage
from PIL import Image, ImageDraw
from os import walk, remove
from os.path import join, exists
import argparse


PATH = '/Users/dmitrypetuhov/Desktop/diploma/code/test_data/'  # to faces with points


def draw_points_at(img, points, r=2, color=(255, 0, 0)):
    img = Image.fromarray(img)
    draw = ImageDraw.Draw(img)
    for (x, y) in points:
        draw.ellipse((x-r, y-r, x+r, y+r), fill=color)
    return np.asarray(img)


def mp_worker((img_path, pts_path)):
    print "Processing {}".format(img_path)
    face_img = misc.imread(img_path)
    face_pts = np.genfromtxt(pts_path, skip_header=2, skip_footer=1)
    face_img = draw_points_at(face_img, face_pts)
    save_to_path = img_path.rpartition('.')[0] + '_pts.png'
    misc.imsave(save_to_path, face_img)


def main(njobs):
    p = multiprocessing.Pool(njobs)
    data = []
    for root, dirs, files in walk(PATH):
        for file in files:
            fname, _, ext = file.partition('.')
            if ext == 'png':
                img_path = join(root, file)
                pts_path = join(root, fname) + '.pts'
                if exists(pts_path):
                    data.append((img_path, pts_path))
    p.map(mp_worker, data)


def clean():
    for root, dirs, files in walk(PATH):
        for file in files:
            fname, _, _ = file.partition('.')
            if fname.endswith('_pts'):
                remove(join(root, file))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--njobs', type=int, default=4, help='number of processes to use')
    parser.add_argument('--path', type=str, default=PATH, help='path to face images and pts files')
    parser.add_argument('--clean', type=int, default=0, help='flag to delete old face images with points')
    args = parser.parse_args()

    if args.path != PATH:
        PATH = args.path

    if args.clean:
        print "Removing old face with points..."
        clean()
        exit()

    main(args.njobs)
