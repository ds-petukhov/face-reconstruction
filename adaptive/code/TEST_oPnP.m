OPNP_3DPOINTS = [ ...
    0, 10,  0, 10,  0, 10,  0, 10;
    0, 0,  10, 10,  0,  0, 10, 10;
    0, 0,   0,  0, 10, 10, 10, 10;
];
N_POINTS = size(OPNP_3DPOINTS, 2);

C = [
    724.264097214, 0,             400;
    0            , 724.264097214, 300;
    0            , 0            , 1;
];

R =  [1, 0, 0; 
      0, 1, 0; 
      0, 0, 1];
assert(isequal(R*R', eye(3,3)));
assert(abs(det(R)) == 1);

t = [0, 0, 10]';

OPNP_2DPOINTS = zeros(2, N_POINTS);
for i = 1:N_POINTS
   X_cam = R*OPNP_3DPOINTS(:, i) + t;
   proj = C*X_cam;
   proj = proj ./ proj(3);
   x = proj(1);
   y = proj(2);
   OPNP_2DPOINTS(:, i) = [x, y]';
end

OPNP_2DPOINTS = inv(C) * [OPNP_2DPOINTS; ones(1, N_POINTS)];
OPNP_2DPOINTS = OPNP_2DPOINTS(1:2, :);

[R0, t0] = OPnP(OPNP_3DPOINTS, OPNP_2DPOINTS);
