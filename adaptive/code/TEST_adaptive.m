function reconstruction = TEST_adaptive(params)

    MODEL             = params.model;
    ALPHA_DIM         = params.alpha_dim;
    LANDMARKS_COUNT   = params.landmarks_count;
    STATIC_INDICES    = params.static_indices;
    LANDMARKS         = params.landmarks;
    ALPHA_SCALE       = params.alpha_scale;
    TRANSLATION_SCALE = params.translation_scale;
    C                 = params.C;

    CONTOUR           = params.contour;
    PATHS             = params.paths;
    N_PATHS           = size(PATHS, 2);

    GTRUTH_ALPHA      = params.gtruth_alpha;
    GTRUTH_R          = params.gtruth_R;
    GTRUTH_t          = params.gtruth_t;

%%%%%%%%%%%%%%%%%%%%%%%%% Make starting point %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    alpha0            = zeros(ALPHA_DIM, 1);
    shape0            = coef2object(alpha0, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);

    R0 = GTRUTH_R;
    t0 = GTRUTH_t;

    rot_vector0 = rodrigues(R0);

    p0 = zeros(N_PATHS, 1);
    for i = 1:N_PATHS
        [cpoints, cnormals] = control_points_and_normals(shape0, PATHS(i));
        path = cscvn(cpoints);
        normal_path = cscvn(cnormals);
        p0(i) = fminbnd(@(x) calc_angle(x, path, normal_path, R0, t0), ...
                           path.breaks(1), path.breaks(end));
    end

    x0 = [alpha0' / ALPHA_SCALE, rot_vector0', t0' / TRANSLATION_SCALE, p0' / TRANSLATION_SCALE];

%%%%%%%%%%%%%%%%%%%%%%%%%%% Test optimum point %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     optimum_point     = [GTRUTH_ALPHA / ALPHA_SCALE, rodrigues(GTRUTH_R)', GTRUTH_t' / TRANSLATION_SCALE, p0' / TRANSLATION_SCALE];
%     optimum_energy    = calc_energy(optimum_point);
%     optimum_th        = 50.0;
%     val               = norm(optimum_energy);
%     assert(val < optimum_th)

%%%%%%%%%%%%%%%%%%%%%%%%%%% Call lsqnonlin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    problem.x0        = x0;
    problem.objective = @calc_energy;
    problem.options   = optimset( ...
                        'Jacobian',         'off'                 ...
                      , 'Display',          'iter'                ...
                      , 'TolFun',            1e-42                ...
                      , 'TolX',              1e-42                ...
    );
    problem.solver    = 'lsqnonlin';
    result            = lsqnonlin(problem);


%%%%%%%%%%%%%%%%%%%%%%%%%%% Return solution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    reconstruction.alpha = result(1:ALPHA_DIM)' * ALPHA_SCALE;
    rot_vector           = result(ALPHA_DIM + 1 : ALPHA_DIM + 3)';
    reconstruction.R     = rodrigues(rot_vector);
    reconstruction.t     = result(ALPHA_DIM + 4 : ALPHA_DIM + 6)' * TRANSLATION_SCALE;

    res_p = result((end-N_PATHS+1):end)' * TRANSLATION_SCALE;
    res_shape = coef2object(reconstruction.alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);
    for i=1:size(res_p, 1)
        [res_cpoints, res_cnormals] = control_points_and_normals(res_shape, PATHS(i));
        res_path = cscvn(res_cpoints);
        res_normal_path = cscvn(res_cnormals);
        res_angle = calc_angle(res_p(i), res_path, res_normal_path, reconstruction.R, reconstruction.t);
        res_angle;

        proj = C*(reconstruction.R*fnval(res_path, res_p(i)) + reconstruction.t);
        proj = proj ./ proj(3);
        proj = proj(1:2);
        res_dist = my_distance2curve(CONTOUR, proj);
        res_dist;
    end

    GTRUTH_ALPHA
    reconstruction.alpha
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% Energy function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function energy = calc_energy(point)
        energy = zeros(1, LANDMARKS_COUNT + 2*N_PATHS);

        cur_alpha      = point(1:ALPHA_DIM)' * ALPHA_SCALE;
%         cur_alpha = GTRUTH_ALPHA;

        cur_rot_vector = point(ALPHA_DIM + 1 : ALPHA_DIM + 3)';
%         cur_t          = point(ALPHA_DIM + 4 : ALPHA_DIM + 6)' * TRANSLATION_SCALE;
        cur_t = GTRUTH_t;
        cur_p          = point((end-N_PATHS+1):end)' * TRANSLATION_SCALE;

        cur_shape      = coef2object(cur_alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);
%         cur_R          = rodrigues(cur_rot_vector);
        cur_R = GTRUTH_R;

        % Project face model using R, t & compute errors
        for i = 1:LANDMARKS_COUNT
            shape_pnt = cur_shape((3*STATIC_INDICES(i) + 1):(3*STATIC_INDICES(i) + 3));
            proj = C*(cur_R*shape_pnt + cur_t);
            proj = proj ./ proj(3);
            x = proj(1);
            y = proj(2);
            energy(1, i) = norm([x;y] - LANDMARKS(:, i));
%             energy(1, 2*i - 1) = x - LANDMARKS(1, i);
%             energy(1, 2*i    ) = y - LANDMARKS(2, i);
        end

        for i = 1:N_PATHS
            [cur_cpoints, cur_cnormals] = control_points_and_normals(cur_shape, PATHS(i));
            cur_path = cscvn(cur_cpoints);
            cur_normal_path = cscvn(cur_cnormals);

            angle = calc_angle(cur_p(i), cur_path, cur_normal_path, cur_R, cur_t);
            energy(1, i + LANDMARKS_COUNT) = angle;

            proj = C*(cur_R*fnval(cur_path, cur_p(i)) + cur_t);
            proj = proj ./ proj(3);
            proj = proj(1:2);
            [min_dist, min_dx, min_dy] = my_distance2curve(CONTOUR, proj);
            energy(1, i + LANDMARKS_COUNT + N_PATHS) = min_dist;
%             energy(1, i + 2*LANDMARKS_COUNT + N_PATHS) = min_dx;
%             energy(1, i + 2*LANDMARKS_COUNT + 2*N_PATHS) = min_dy;
        end
    end

    function angle = calc_angle(p, path, normal_path, R, t)
        point  = fnval(path, p);
        np = normal_path.breaks(1) ...
               + (p / (path.breaks(end) - path.breaks(1)) ) ...
               * (normal_path.breaks(end) - normal_path.breaks(1));
        normal = fnval(normal_path, np);
        point_proj = R*point + t;
        angle = abs(90 - atan2d(norm(cross(normal,point_proj)),dot(normal,point_proj)));
%         angle = dot(normal,point_proj);
    end

    function [min_dist, min_dx, min_dy] = my_distance2curve(CONTOUR, point)
        min_dist = norm(CONTOUR(:, 1) - point);
        min_dx = CONTOUR(1, 1) - point(1);
        min_dy = CONTOUR(2, 1) - point(2);
        for q=2:size(CONTOUR, 2)
            dist_q = norm(CONTOUR(:, q) - point);
            if dist_q < min_dist
                min_dist = dist_q;
                min_dx = CONTOUR(1, q) - point(1);
                min_dy = CONTOUR(2, q) - point(2);
            end
        end
    end
end
