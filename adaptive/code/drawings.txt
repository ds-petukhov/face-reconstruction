figure;

set(0,'DefaultAxesFontSize',11,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',11,'DefaultTextFontName','Times New Roman');

title('Зависимость качества реконструкции от угла поворота');
xlabel('Угол (°)');

title('Зависимость качества реконструкции от коэффициента регуляризации');
xlabel('Коэффициент регуляризации');

plot(ANGLES, shape_avg_errors);
ylabel('Ошибка (мм.)');

ax = gca;
ax.XTick = ANGLES;

ylabel('Средняя ошибка по альфе (сумма модулей разностей)');
ylabel('Средняя ошибка перепроецирования (пиксели)');
-----

figure;

set(0,'DefaultAxesFontSize',14,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',14,'DefaultTextFontName','Times New Roman');

title('Зависимость качества реконструкции от угла поворота')
xlabel('Угол (°)')

yyaxis left;
plot(ANGLES, shape_avg_errors);
ylabel('Ошибка (мм.)')

ax = gca;
ax.XTick = ANGLES;

-----

yyaxis right;
plot(ANGLES, reprojection_avg_errors);
ylabel('?????? ?????????????????? (????.)');

----- REGULARIZATION ------

TMP = ADAPT
figure;

set(0,'DefaultAxesFontSize',14,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',14,'DefaultTextFontName','Times New Roman');

xlabel('Коэффициент регуляризации');

yyaxis left;
semilogx(TMP.REGS, TMP.norm_angle_avg_errors, 'LineWidth', 2);
ylabel('$Err_{norm}$','Interpreter','LaTex')

yyaxis right;
semilogx(TMP.REGS, TMP.reprojection_avg_errors, 'LineWidth', 2);
ylabel('$Err_{proj}$','Interpreter','LaTex')

ax = gca;
ax.XTick = TMP.REGS;

---- BY ANGLE -----
BASIC = load('experiments/experiment6/exp_data_basic_bfm.mat');
ADAPT = load('experiments/experiment6/exp_data_adaptive_bfm.mat');
ANGLES = [-30, -20, -10, 0, 10, 20, 30];

figure;
set(0,'DefaultAxesFontSize',14,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',14,'DefaultTextFontName','Times New Roman');

xlabel('Угол (градусы)');
ylabel('$Err_{rot}$','Interpreter','LaTex')

VALUES_BASIC = [];
VALUES_ADAPT = [];
for ai = 1:size(BASIC.ANGLE2INX, 1)
    inx_b = BASIC.ANGLE2INX(ai, BASIC.ANGLE2INX(ai, :) ~= 0);
    VALUES_BASIC(ai) = mean(BASIC.RESULTS.translation_error(inx_b));

    inx_a = ADAPT.ANGLE2INX(ai, ADAPT.ANGLE2INX(ai, :) ~= 0);
    VALUES_ADAPT(ai) = mean(ADAPT.RESULTS.translation_error(inx_a));
end
hold on;
plot(ANGLES, VALUES_BASIC, 'b', 'LineWidth', 2);
plot(ANGLES, VALUES_ADAPT, 'g', 'LineWidth', 2);
legend('Фиксированный', 'Предложенный');
hold off;


    print2log(LOG, RESULTS.reprojection_error(inx), RESULTS.alpha_error(inx), RESULTS.shape_error(inx), ...
                   RESULTS.rotation_error(inx), RESULTS.translation_error(inx), ...
                   RESULTS.smart_alpha_error(inx), RESULTS.rmse_shape_error(inx), ...
                   RESULTS.norm_angle_error(inx));
end


------------------

figure;

set(0,'DefaultAxesFontSize',14,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',14,'DefaultTextFontName','Times New Roman');

xlabel('Коэффициент регуляризации');

yyaxis left;
semilogx(REGS, BASIC.rot_avg_errors, 'LineWidth', 2);
ylabel('$Err_{R}$','Interpreter','LaTex')

yyaxis right;
semilogx(REGS, BASIC.translate_avg_errors, 'LineWidth', 2);
ylabel('$Err_{t}$','Interpreter','LaTex')

ax = gca;
ax.XTick = REGS;

----- SMART ---

figure;

set(0,'DefaultAxesFontSize',10,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',10,'DefaultTextFontName','Times New Roman');

title('Зависимость качества реконструкции от коэффициента регуляризации');
xlabel('Коэффициент регуляризации');

yyaxis left;
semilogx(TMP.REGS, TMP.norm_angle_avg_errors);
ylabel('Средняя ошибка нормали');

yyaxis right;
semilogx(TMP.REGS, TMP.reprojection_avg_errors);
ylabel('Средняя ошибка перепроецирования (пиксели)');

ax = gca;
ax.XTick = TMP.REGS;

bas 6.6764e+11
ad d5 6.692301e+11
ad d10 6.095163e+11

----- rmse_errors -----

figure;

set(0,'DefaultAxesFontSize',11,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',11,'DefaultTextFontName','Times New Roman');

title('Зависимость качества реконструкции от коэффициента регуляризации');
xlabel('Коэффициент регуляризации');

yyaxis left;
semilogx(REGS, rmse_errors);
ylabel('Средняя ошибка по альфе');

yyaxis right;
semilogx(REGS, reprojection_avg_errors);
ylabel('Средняя ошибка перепроецирования (пиксели)');

ax = gca;
ax.XTick = REGS;

----
img_path = '/Users/dmitrypetuhov/Desktop/diploma/code/experiments/iterX.png';
system(strcat('/Users/dmitrypetuhov/Desktop/diploma/code/render_me.py "', mat2str(reconstruction.alpha), '" "', img_path, '"'));

----

save('errors_basic.mat', 'alpha_avg_errors', 'reprojection_avg_errors', 'shape_avg_errors', 'smart_alpha_avg_errors', 'rmse_errors', 'normal_avg_errors', 'rot_avg_errors',  'translate_avg_errors')

save('errors_ad2.mat', 'alpha_avg_errors', 'reprojection_avg_errors', 'shape_avg_errors', 'smart_alpha_avg_errors', 'rmse_errors', 'normal_avg_errors', 'rot_avg_errors',  'translate_avg_errors')
