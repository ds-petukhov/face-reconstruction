function [ img, path ] = get_render_img(shp, model, width, height, iter, USE_ORTHO)

    shp = reshape(shp, [ 3 prod(size(shp))/3 ])'; 
    tex = coef2object(zeros(size(model.texPC, 2), 1),  model.texMU,   model.texPC,   model.texEV);
    tex = reshape(tex, [ 3 prod(size(tex))/3 ])'; 
    tex = min(tex, 255);
    tl = model.tl;
    
    FOV = 45; % Really?
    
    set(gcf, 'Renderer', 'opengl');
    set(gcf, 'Units', 'pixels');
    set(gcf, 'PaperUnits','inches','PaperPosition',[0 0 width height] / 150)
    camva(FOV);  % Set the camera field of view
    
    fig_pos = get(gcf, 'Position');
    fig_pos(1) = 1;
    fig_pos(2) = 1;
    fig_pos(3) = width;
    fig_pos(4) = height;
    set(gcf, 'Position', fig_pos);

    mesh_h = trimesh(...
        tl, shp(:, 1), shp(:, 3), shp(:, 2), ...
        'EdgeColor', 'none', ...
        'FaceVertexCData', tex/255, ...
        'FaceColor', 'interp' ...
    );
    
    if USE_ORTHO
        set(gca, ...
            'DataAspectRatio', [ 1 1 1 ], ...
            'PlotBoxAspectRatio', [ 1 1 1 ], ...
            'Units', 'pixels', ...
            'GridLineStyle', 'none', ...
            'Position', [ 0 0 fig_pos(3) fig_pos(4) ], ...
            'Visible', 'off', 'box', 'off', ...
            'Projection', 'orthographic' ...
            );
    else
        set(gca, ...
            'DataAspectRatio', [ 1 1 1 ], ...
            'PlotBoxAspectRatio', [ 1 1 1 ], ...
            'Units', 'pixels', ...
            'GridLineStyle', 'none', ...
            'Position', [ 0 0 fig_pos(3) fig_pos(4) ], ...
            'Visible', 'off', 'box', 'off', ...
            'Projection', 'perspective' ...
            );
    end
    
    set(gcf, 'Color', [ 0 0 0 ]); 
    view(180, 0);
    

    material([.5, .5, .1 1  ]);
    camlight('headlight');

    set(gcf, 'visible', 'off');
    
    path = '/Users/dmitrypetuhov/Desktop/diploma/code/experiments/iter';
    path = strcat(path, num2str(iter), '.png');
%     saveas(gcf, '/Users/dmitrypetuhov/Desktop/diploma/code/experiments/iter.png');
%     print('/Users/dmitrypetuhov/Desktop/diploma/code/experiments/iter.png', '-dpng', '-r100')
    print(gcf, '-dpng', '-r150', path);
    img = imread(path);
%     F = getframe;
%     [img, ~] = frame2im(F);
end

