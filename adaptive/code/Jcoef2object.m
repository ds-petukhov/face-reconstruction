function values = Jcoef2object(pc, ev, n_dim)    
    values = zeros(size(pc, 1), n_dim);
    for j = 1:n_dim
        my_ev = zeros(n_dim, 1);
        my_ev(j) = ev(j);
        values(:, j) = pc(:, 1:n_dim) * my_ev;
    end
end