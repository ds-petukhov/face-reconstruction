#!/usr/bin/python
import matlab.engine
import os

from subprocess import call

from render_face import Renderer
from PIL import Image
from render_face import Renderer
from PIL import Image, ImageDraw
from scipy import misc, ndimage
import numpy as np

import dlib
from skimage import io
from skimage.draw import circle

MATLAB_PROJECT_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code"

LAND_COUNT = 68
WIDTH = 1200;
HEIGHT = 900;
PREDICTOR_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code/extras/shape_predictor_68_face_landmarks.dat"
DETECTOR = dlib.get_frontal_face_detector()
PREDICTOR = dlib.shape_predictor(PREDICTOR_PATH)

FACE_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code/experiments/avg_face.png"
POINTS_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code/experiments/static_indecies.txt"

def main():
    print "Initializing matlab...",
    eng = matlab.engine.start_matlab()
    eng.addpath(eng.genpath(MATLAB_PROJECT_PATH))
    print "OK!"

    print "Generate average face...",
    alpha0 = eng.zeros(1, 1)
    shape, tex, tl = eng.gen_face(alpha0, nargout=3)
    print "OK!"

    print "Init renderer...",
    renderer = Renderer(WIDTH, HEIGHT, useOrtho=True)
    renderer.set_texture(tex)
    renderer.set_tl(tl)
    print "OK!"

    print "Rendering...",
    image = renderer.render_shape(shape)
    print "OK!"

    print "Saving the image...",
    image.save(FACE_PATH)
    print "OK!"

    img = io.imread(FACE_PATH)
    print "Detecting landmarks...",
    dets = DETECTOR(img, 0)
    landmarks = PREDICTOR(img, dets[0])
    print "OK!"

    stable_landmarks_list = []
    print "Draw stable landmarks on the image...",
    for j in xrange(0, LAND_COUNT):
        p = landmarks.part(j)
        if j > 16:
            stable_landmarks_list.append((p.x, p.y))
            rr, cc = circle(p.y, p.x, 2)
            img[rr, cc] = [0, 255, 0]
    misc.imsave(FACE_PATH, img)
    print "OK!"

    with open(POINTS_PATH, 'w') as f:
        indecies = renderer.model_indecies(stable_landmarks_list)
        for idx in indecies:
            f.write(str(idx) + "\n")

if __name__ == "__main__":
    main()
