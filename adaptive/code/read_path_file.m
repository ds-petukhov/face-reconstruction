function [ path ] = read_path_file( fname )
%READ_PATH_FILE Read path file
%   Path file should have the following structure:
%   For each path's control point goes one line as following:
%       vertex_id1, vertex_id2, vertex_id3, alpha, beta, gamma

    file = fopen(fname, 'r');
    filedata = textscan(file, '%f', 'delimiter', ',', 'HeaderLines', 0);
    filedata = filedata{:};
    
    path         = struct();
    path.indices = [];
    path.coords  = [];
    
    n_control_points = size(filedata, 1) / 6;
    for i = 1:n_control_points
        path.indices = [path.indices, filedata(6*(i-1) + 1 : 6*(i-1) + 3)];
        path.coords  = [path.coords, filedata(6*(i-1) + 4 : 6*(i-1) + 6)];
    end
    
    fclose(file);
end

