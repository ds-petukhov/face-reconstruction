% 1. Run mk_experiment.py
% 2. Run get_static_indices.py
% 3. Run this script
clear;
EXPERIMENTS_PATH      = './experiments/*.proj';
MODEL_PATH            = './bfm/MorphableModel.mat';
INDICES_PATH          = './experiments/static_indecies.txt';
PATHS_PATH            = './paths/*.txt';

PATHS                 = dir(PATHS_PATH);
PROJECTS              = dir(EXPERIMENTS_PATH);
MODEL                 = load(MODEL_PATH);

ALPHA_SCALE           = 1e8;
TRANSLATION_SCALE     = 1e8;
ROTATION_SCALE        = 1e8;
LOCATION_SCALE        = 1e8;

N                     = size(PROJECTS, 1);

USE_ORTHO             = true;

% Read path's control points
PATHS_CONTROL_POINTS = [];
for k=1:size(PATHS, 1)
    PATHS_CONTROL_POINTS = [PATHS_CONTROL_POINTS, read_path_file(PATHS(k).name)];
end

TEST_TEXTURE  = coef2object(zeros(size(MODEL.texPC, 2), 1),  MODEL.texMU,   MODEL.texPC,   MODEL.texEV);

REGS = [0, 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 1e1];
alpha_avg_errors        = zeros(1, length(REGS));
smart_alpha_avg_errors  = zeros(1, length(REGS));
reprojection_avg_errors = zeros(1, length(REGS));
shape_avg_errors        = zeros(1, length(REGS));
rmse_errors             = zeros(1, length(REGS));
normal_avg_errors       = zeros(1, length(REGS));
rot_avg_errors          = zeros(1, length(REGS));
translate_avg_errors    = zeros(1, length(REGS));

for k = 1:length(REGS)

    alpha_avg_err = 0.0;
    smart_alpha_avg_err = 0.0;
    reprojection_avg_err = 0.0;
    shape_avg_err = 0.0;
    rmse_err = 0.0;
    normal_avg_err = 0.0;
    rot_avg_err = 0.0;
    translate_avg_err = 0.0;

    for i=1:N
        display(i);
        display(PROJECTS(i).name);
        project = read_project(PROJECTS(i).name);

        % Choose landmarks & indices
        static_indices = read_indecies(INDICES_PATH);
        landmarks = project.landmarks(:, 18:end);
        assert(size(static_indices, 1) == size(landmarks, 2));

        % Make a contour from unstable landmarks
        unstable_landmarks = project.landmarks(:, 1:17);
        contour = [];
        for l = 1:(size(unstable_landmarks, 2)-1)
           x1 = unstable_landmarks(1, l);
           y1 = unstable_landmarks(2, l);
           x2 = unstable_landmarks(1, l+1);
           y2 = unstable_landmarks(2, l+1);
           [xs, ys] = bresenham(x1,y1,x2,y2);
           xy = [xs'; ys'];
           contour = [contour, xy];
        end

        % Prepare params
        width  = project.width;
        height = project.height;
        fW     = project.fW;
        fH     = project.fH;

        params    = struct();
        % Basic params goes here:
        if USE_ORTHO
            params.C  = [fW, 0., 0.;
                         0., fH, 0.;
                         0., 0., 0.];
        else
            params.C  = [fW, 0., width/2.;
                         0., fH, height/2.;
                         0., 0., 1.];
        end

        params.use_ortho         = USE_ORTHO;
        params.width             = width;
        params.height            = height;
        params.R                 = project.gtruth_rotation;
        params.t                 = project.gtruth_translation;

        params.alpha_dim         = project.alpha_dim;
        params.landmarks         = landmarks;
        params.landmarks_count   = size(landmarks, 2);
        params.static_indices    = static_indices;
        params.model             = MODEL;
        params.alpha_scale       = ALPHA_SCALE;
        params.translation_scale = TRANSLATION_SCALE;
        params.location_scale    = LOCATION_SCALE;
        params.rotation_scale    = ROTATION_SCALE;

        % Additional params for adaptive method
        params.contour           = contour;
        params.paths             = PATHS_CONTROL_POINTS;

        params.reg = REGS(k);

        % Basic algorithm
%         reconstruction = basic(params);

        % Adaptive algorithm
        params.land_scale = 100;
        params.angle_scale = 50;
        params.dist_scale = 10;
        reconstruction = adaptive(params);

        % Alpha error
        alpha_avg_err = alpha_avg_err + sum(abs((reconstruction.alpha - project.gtruth_alpha)));
        smart_alpha_avg_err = smart_alpha_avg_err + sum( (MODEL.shapeEV(1:params.alpha_dim) .* (reconstruction.alpha - project.gtruth_alpha)) .^2 );

        % Reprojection error
        error = 0.0;
        rec_shape = coef2object(reconstruction.alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);
        for j = 1:params.landmarks_count
           X      = rec_shape((3*params.static_indices(j) + 1):(3*params.static_indices(j) + 3));
           [x, y] = project_point(X, params.C, params.width, params.height, USE_ORTHO, reconstruction.R, reconstruction.t);
           proj  = [x; y];
           error  = error + norm(params.landmarks(:, j) - proj(1:2));
        end
        reprojection_avg_err = reprojection_avg_err + error / params.landmarks_count;

        % Shape error
        gtruth_shape = coef2object(project.gtruth_alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);
        n_vertex     = size(gtruth_shape, 1) / 3;
        shape_error  = 0.0;
        sum_rmse = 0.0;
        for j = 1:n_vertex
           e = norm(gtruth_shape((3*(j-1)+1):(3*(j-1)+3)) - rec_shape((3*(j-1)+1):(3*(j-1)+3)));
           shape_error = shape_error + e;
           sum_rmse = sum_rmse + e^2;
        end
        shape_avg_err = shape_avg_err + (shape_error / n_vertex);
        rmse_err = rmse_err + sqrt(sum_rmse / n_vertex);

        % Normal error
        norm_angles_sum = 0;
        for j = 1:size(MODEL.tl, 1)
            id1 = MODEL.tl(j, 1);
            id2 = MODEL.tl(j, 2);
            id3 = MODEL.tl(j, 3);

            A_truth = gtruth_shape(3*(id1 - 1) + 1 : 3*(id1 - 1) + 3);
            B_truth = gtruth_shape(3*(id2 - 1) + 1 : 3*(id2 - 1) + 3);
            C_truth = gtruth_shape(3*(id3 - 1) + 1 : 3*(id3 - 1) + 3);

            A_rec = rec_shape(3*(id1 - 1) + 1 : 3*(id1 - 1) + 3);
            B_rec = rec_shape(3*(id2 - 1) + 1 : 3*(id2 - 1) + 3);
            C_rec = rec_shape(3*(id3 - 1) + 1 : 3*(id3 - 1) + 3);

            normal_truth = cross(B_truth - A_truth, C_truth - A_truth);
            normal_truth = normal_truth / norm(normal_truth);

            normal_rec = cross(B_rec - A_rec, C_rec - A_rec);
            normal_rec = normal_rec / norm(normal_rec);

            norm_angles_sum = norm_angles_sum + atan2(norm(cross(normal_truth, normal_rec)), dot(normal_truth, normal_rec)) * 180 / pi;
        end
        normal_avg_err = normal_avg_err + norm_angles_sum / size(MODEL.tl, 1);

        % Rotation error
        R_true = project.gtruth_rotation;
        R_rec  = reconstruction.R;
        rot_err = acos(dot(R_true(:, 1), R_rec(:, 1))) * 180 / pi;
        rot_err = max(rot_err, acos(dot(R_true(:, 2), R_rec(:, 2))) * 180 / pi);
        rot_avg_err = rot_avg_err + rot_err;

        % Translation error : e = ||t_true - t||
        translate_avg_err = translate_avg_err + norm(project.gtruth_translation(1:2) - reconstruction.t(1:2));
    end

    alpha_avg_errors(k) = alpha_avg_err / N;
    reprojection_avg_errors(k) = reprojection_avg_err / N;
    shape_avg_errors(k) = shape_avg_err / N;
    smart_alpha_avg_errors(k) = smart_alpha_avg_err / N;
    rmse_errors(k) = rmse_err / N;
    normal_avg_errors(k) = normal_avg_err / N;
    rot_avg_errors(k) = rot_avg_err / N;
    translate_avg_errors(k) = translate_avg_err / N;
end

figure;

set(0,'DefaultAxesFontSize',11,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',11,'DefaultTextFontName','Times New Roman');

title('??????????? ???????? ????????????? ?? ???????????? ?????????????');
xlabel('??????????? ?????????????');

yyaxis left;
semilogx(REGS, alpha_avg_errors);
ylabel('??????? ?????? ?? ????? (????? ??????? ?????????)');

yyaxis right;
semilogx(REGS, reprojection_avg_errors);
ylabel('??????? ?????? ????????????????? (???????)');

ax = gca;
ax.XTick = REGS;

5;
