#!/usr/bin/python

from subprocess import call
from render_face import Renderer
from PIL import Image, ImageDraw
from scipy import misc, ndimage

import matlab.engine
import numpy as np
import dlib

from skimage import io
from skimage.draw import circle

import os
import sys
import time

EXPERIMENTS_COUNT = 100;
WIDTH = 1200;
HEIGHT = 900;
ALPHA_MEAN = 0.0;
ALPHA_VAR = 1.0;
MIN_ANGLE = 30;
MAX_ANGLE = 30;
# NOISE_LVL = 10;

MATLAB_PROJECT_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code"

LAND_COUNT = 68
PREDICTOR_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code/extras/shape_predictor_68_face_landmarks.dat"
DETECTOR = dlib.get_frontal_face_detector()
PREDICTOR = dlib.shape_predictor(PREDICTOR_PATH)

def landmarks2str(landmarks):
    landmarks_str = ""
    for i in xrange(0, LAND_COUNT):
        p = landmarks.part(i)
        landmarks_str = landmarks_str + str(p.x) + ":" + str(p.y) + ","
    return landmarks_str[:-1]


def mk_project_file(img_path, landmarks, landmarks_count, alpha_dim, alpha, focal_length, R, t):
    with open(img_path[:-4] + ".proj", 'w') as f:
        f.write("proj\n")
        f.write("format ascii 1.0\n")
        f.write("image:\n")
        f.write(img_path + '\n')
        f.write("resolution:\n")
        f.write("{},{}".format(WIDTH, HEIGHT) + '\n')
        f.write("focal_length:\n")
        f.write("{},{}\n".format(focal_length[0], focal_length[1]))
        f.write("landmarks_count:\n")
        f.write(str(landmarks_count) + '\n')
        f.write("landmarks:\n")
        f.write(str(landmarks) + '\n')
        f.write("alpha_dim:\n")
        f.write(str(alpha_dim) + '\n')
        f.write("true_alpha:\n")
        f.write(str(alpha) + '\n')
        f.write("true_rotation:\n")
        f.write("{},{},{}\n".format(R[0][0], R[0][1], R[0][2]))
        f.write("{},{},{}\n".format(R[1][0], R[1][1], R[1][2]))
        f.write("{},{},{}\n".format(R[2][0], R[2][1], R[2][2]))
        f.write("true_translation:\n")
        f.write("{},{},{}\n".format(t[0], t[1], t[2]))


def main():
    print "Initializing matlab..."
    eng = matlab.engine.start_matlab()
    eng.addpath(eng.genpath(MATLAB_PROJECT_PATH))

    print "Starting %i experiments..." % EXPERIMENTS_COUNT
    alphas, shapes, tex, tl = eng.rand_shape(ALPHA_MEAN, ALPHA_VAR, EXPERIMENTS_COUNT, nargout=4)
    print "done."

    renderer = Renderer(WIDTH, HEIGHT, minAngle=MIN_ANGLE, maxAngle=MAX_ANGLE, useOrtho=True)
    renderer.set_texture(tex)
    renderer.set_tl(tl)

    paths = []
    Rs = []
    ts = []
    fs = []
    for i, shape in enumerate(shapes):
        sys.stdout.flush()
        time.sleep(1)
        print "Render", i, '\r',
        image = renderer.render_shape(shape)
        paths.append("/Users/dmitrypetuhov/Desktop/diploma/code/experiments/face"+str(i)+"_" + str(MAX_ANGLE) +".png")
        Rs.append(renderer.get_rotation_matrix())
        ts.append(renderer.get_translation_vector())
        fs.append(renderer.get_focal_length())
        image.save(paths[-1])

    alpha_dim = len(alphas[0])

    for i, img_path in enumerate(paths):
        img = io.imread(img_path)

        print "Detecting landmarks %i..." % i,
        dets = DETECTOR(img, 1)
        landmarks = PREDICTOR(img, dets[0])
        print "OK!"

        print "Draw landmarks on the image...",
        for j in xrange(0, LAND_COUNT):
            p = landmarks.part(j)
            rr, cc = circle(p.y, p.x, 2)
            img[rr, cc] = [0, 255, 0]
        misc.imsave(img_path, img)
        print "OK!"

        print "Making proj file %i..." % i,
        landmarks_str = landmarks2str(landmarks)
        alpha = str(alphas[i])[1:-1] # drop '[' and ']'
        mk_project_file(img_path, landmarks_str, LAND_COUNT, alpha_dim, alpha, fs[i], Rs[i], ts[i])
        print "OK!"


if __name__ == "__main__":
    main()
