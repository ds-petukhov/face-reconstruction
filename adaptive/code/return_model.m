function [  tl ] = return_model()
%RETURN_MODEL Summary of this function goes here
%   Detailed explanation goes here
    
    MODEL_PATH            = './bfm/MorphableModel.mat';
    MODEL                 = load(MODEL_PATH);
    mu =  MODEL.texMU;
    pc =  MODEL.texPC;
    ev = MODEL.texEV;
    tl = MODEL.tl;
end

