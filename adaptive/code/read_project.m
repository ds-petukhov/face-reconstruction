function project = read_project(file)

    %READ_PROJECT Reads project from a file

    project.file = file;

    fid = fopen(file);
    rootdir = fileparts(file);

    magic = fgetl(fid);
    assert(strcmp(magic, 'proj'));
    format = fgetl(fid);
    assert(strcmp(format, 'format ascii 1.0'));

    assert(strcmp(fgetl(fid), 'image:'));
    project.image_path = fgetl(fid);

    assert(strcmp(fgetl(fid), 'resolution:'));
    image_size = textscan(fgetl(fid), '%f', 'delimiter', ',');
    image_size = image_size{1};
    project.width = image_size(1);
    project.height = image_size(2);

    assert(strcmp(fgetl(fid), 'focal_length:'));
    f = textscan(fgetl(fid), '%f', 'delimiter', ',');
    f = f{1};
    project.fW = f(1);
    project.fH = f(2);

    assert(strcmp(fgetl(fid), 'landmarks_count:'));
    n_landmarks = textscan(fgetl(fid), '%d');
    project.n_landmarks = n_landmarks{1};

    assert(strcmp(fgetl(fid), 'landmarks:'));
    landmarks = textscan(fgetl(fid), '%f:%f', 'delimiter', ',');
    landmarks = [landmarks{1}, landmarks{2}]';
    assert(project.n_landmarks == size(landmarks, 2));
    project.landmarks = landmarks;

    assert(strcmp(fgetl(fid), 'alpha_dim:'));
    alpha_dim = textscan(fgetl(fid), '%d');
    project.alpha_dim = alpha_dim{1};

    assert(strcmp(fgetl(fid), 'true_alpha:'));
    gtruth_alpha = textscan(fgetl(fid), '%f', 'delimiter', ',');
    gtruth_alpha = gtruth_alpha{1};
    assert(project.alpha_dim == size(gtruth_alpha, 1));
    project.gtruth_alpha = gtruth_alpha;

    assert(strcmp(fgetl(fid), 'true_rotation:'));
    gtruth_rotation_f = textscan(fgetl(fid), '%f', 'delimiter', ',');
    gtruth_rotation_s = textscan(fgetl(fid), '%f', 'delimiter', ',');
    gtruth_rotation_t = textscan(fgetl(fid), '%f', 'delimiter', ',');
    project.gtruth_rotation = [ ...
        gtruth_rotation_f{1}';
        gtruth_rotation_s{1}';
        gtruth_rotation_t{1}'
    ];

    assert(strcmp(fgetl(fid), 'true_translation:'));
    gtruth_translation = textscan(fgetl(fid), '%f', 'delimiter', ',');
    project.gtruth_translation = gtruth_translation{1};

    fclose(fid);

end

