function reconstruction = basic(params)

    MODEL             = params.model;
    ALPHA_DIM         = params.alpha_dim;
    LANDMARKS_COUNT   = params.landmarks_count;
    STATIC_INDICES    = params.static_indices;
    LANDMARKS         = params.landmarks;
    ALPHA_SCALE       = params.alpha_scale;
    TRANSLATION_SCALE = params.translation_scale;
    ROTATION_SCALE    = params.rotation_scale;
    C                 = params.C;

    USE_ORTHO         = params.use_ortho;
    WIDTH             = params.width;
    HEIGHT            = params.height;
    TRUE_R            = params.R;
    TRUE_t            = params.t;

%%%%%%%%%%%%%%%%%%%%%%%%% Make starting point %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    alpha0            = zeros(ALPHA_DIM, 1);
    shape0            = coef2object(alpha0, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);

    if USE_ORTHO
%         pts3d = zeros(3, LANDMARKS_COUNT);
%         for i = 1:size(STATIC_INDICES)
%             pts3d(:, i) = shape0((3*STATIC_INDICES(i) + 1):(3*STATIC_INDICES(i) + 3));
%         end
%         K  = my_estimateExtrinsic(LANDMARKS, pts3d);
        R0 = [1 0 0; 0 -1 0; 0 0 -1];
        t0 = [0; 0; 0];
%         fx = K(1,1);
%         fy = K(2,2);
    else
        opnp_3dpts = zeros(3, LANDMARKS_COUNT);
        for i = 1:size(STATIC_INDICES)
            opnp_3dpts(:, i) = shape0((3*STATIC_INDICES(i) + 1):(3*STATIC_INDICES(i) + 3));
        end

        opnp_2dpts = inv(C) * [LANDMARKS; ones(1, LANDMARKS_COUNT)];
        opnp_2dpts = opnp_2dpts(1:2, :);

        [R0, t0]    = OPnP(opnp_3dpts / 1e4, opnp_2dpts);
        t0          = t0 * 1e4;
    end
    rot_vector0 = rodrigues(R0);
    x0 = [alpha0' / ALPHA_SCALE, rot_vector0' / ROTATION_SCALE, t0' / TRANSLATION_SCALE];

%%%%%%%%%%%%%%%%%%%%%%%%%%% Call lsqnonlin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    problem.x0        = x0;
    problem.objective = @calc_energy;
    problem.options   = optimset( ...
                        'Algorithm',        'levenberg-marquardt' ...
                        'Jacobian',         'off'                 ...
                      , 'Display',          'iter'                ...
                      , 'TolFun',            1e-42                ...
                      , 'TolX',              1e-42                ...
                      , 'MaxIter',           20                   ...
                      , 'UseParallel',       true                 ...
    );
    problem.solver    = 'lsqnonlin';
    result            = lsqnonlin(problem);


%%%%%%%%%%%%%%%%%%%%%%%%%%% Return solution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    reconstruction.alpha = result(1:ALPHA_DIM)' * ALPHA_SCALE;
    reconstruction.R     = rodrigues(result(ALPHA_DIM + 1 : ALPHA_DIM + 3)' * ROTATION_SCALE);
    reconstruction.t     = result((end-2):end)' * TRANSLATION_SCALE;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% Energy function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function [energy] = calc_energy(point)
        energy_dim = 2*LANDMARKS_COUNT + ALPHA_DIM;
        energy = zeros(1, energy_dim);

        cur_alpha      = point(1:ALPHA_DIM)' * ALPHA_SCALE;
        cur_R          = rodrigues(point(ALPHA_DIM + 1 : ALPHA_DIM + 3)' * ROTATION_SCALE);

        cur_t          = point((end-2):end)' * TRANSLATION_SCALE;
        cur_shape      = coef2object(cur_alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);


        % Project face model using R, t & compute errors
        for i = 1:LANDMARKS_COUNT
            shape_pnt = cur_shape((3*STATIC_INDICES(i) + 1):(3*STATIC_INDICES(i) + 3));
            [x, y] = project_point(shape_pnt, C, WIDTH, HEIGHT, USE_ORTHO, cur_R, cur_t);
            energy(1, 2*i - 1) = (x - LANDMARKS(1, i)) * 100;
            energy(1, 2*i    ) = (y - LANDMARKS(2, i)) * 100;
        end

        energy(1, 1 + 2*LANDMARKS_COUNT : end) = params.reg .* (MODEL.shapeEV(1:ALPHA_DIM) .* cur_alpha);
    end
end
