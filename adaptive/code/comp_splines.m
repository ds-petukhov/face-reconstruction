function [ result ] = comp_splines( alpha, R, t, shape, paths)
    n_paths       = size(paths, 2);
    
    result.alpha   = alpha;
    result.R       = R;
    result.t       = t;
%     result.splines - splines(X, Y) means path X, Y can be spline for xs, ys, zs, nxs, nys, nzs.
    
    for i = 1:n_paths
        [cpoints, cnormals] = control_points_and_normals(shape, paths(i), result.R, result.t);
        num_points = size(cpoints, 2);
        
        correspondences_table       = zeros(num_points, 7);
        correspondences_table(:, 1) = [0:1/(num_points-1):1]'; % param range [0, 1]
        
        correspondences_table(:, 2) = cpoints(1, :)'; % xs
        correspondences_table(:, 3) = cpoints(2, :)'; % ys
        correspondences_table(:, 4) = cpoints(3, :)'; % zs
        
        correspondences_table(:, 5) = cnormals(1, :)'; % n_xs
        correspondences_table(:, 6) = cnormals(2, :)'; % n_ys
        correspondences_table(:, 7) = cnormals(3, :)'; % n_zs
        
        for j = 1:6
            result.splines(i, j) = spline(correspondences_table(:, 1), correspondences_table(:, j+1));
        end
    end
end

