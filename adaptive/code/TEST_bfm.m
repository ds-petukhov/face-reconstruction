clear;
MODEL_PATH            = './bfm/MorphableModel.mat';
INDICES_PATH          = './experiments/static_indecies.txt';
PATHS_PATH            = './paths/*.txt';

PATHS                 = dir(PATHS_PATH);

MODEL                 = load(MODEL_PATH);

ALPHA_SCALE           = 1e8;
TRANSLATION_SCALE     = 1e8;

USE_ORTHO = true;

TO_MM_FACTOR          = mm_factor(MODEL);

TEST_FACES_PATHS = ...
    { ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/0.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/1.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/2.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/3.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/4.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/5.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/6.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/7.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/8.mat', ...
        '/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans/9.mat', ...
    };

% *0.proj = -30, *1.proj = -20, ... , *5.proj = 20, *6.proj = 30
% ANGLES = [-30, -20, -10, 0, 10, 20, 30];
% EXPERIMENTS_PATH_BY_ANGLE = ...
%     { ...
%         './experiments/*0.proj', ...
%         './experiments/*1.proj', ...
%         './experiments/*2.proj', ...
%         './experiments/*3.proj', ...
%         './experiments/*4.proj', ...
%         './experiments/*5.proj', ...
%         './experiments/*6.proj', ...
%     };
ANGLES = [0, 10, 20, 30];
EXPERIMENTS_PATH_BY_ANGLE = ...
    { ...
        './experiments/*3.proj', ...
        './experiments/*4.proj', ...
        './experiments/*5.proj', ...
        './experiments/*6.proj', ...
    };

% Read path's control points
PATHS_CONTROL_POINTS = [];
for k=1:size(PATHS, 1)
    PATHS_CONTROL_POINTS = [PATHS_CONTROL_POINTS, read_path_file(PATHS(k).name)];
end

alpha_avg_errors        = zeros(1, length(ANGLES));
reprojection_avg_errors = zeros(1, length(ANGLES));
shape_avg_errors        = zeros(1, length(ANGLES));

for k = 1:length(ANGLES)
    
    alpha_avg_err = 0.0;
    reprojection_avg_err = 0.0;
    shape_avg_err = 0.0;
    
    PROJECTS = dir(EXPERIMENTS_PATH_BY_ANGLE{1, k});
    N = size(PROJECTS, 1);
    
    for i=1:N
        display(i);
        display(PROJECTS(i).name);
        project = read_project(PROJECTS(i).name);

        % Choose landmarks & indices
        STABLE_ONLY = false;
        indices = read_indecies(INDICES_PATH);
        static_indices = indices;
        landmarks = project.landmarks;
        if STABLE_ONLY
            landmarks = project.landmarks(:, 18:end);
            static_indices = indices(18:end);
        end
        assert(size(static_indices, 1) == size(landmarks, 2));

        % Make a contour from unstable landmarks
        unstable_landmarks = project.landmarks(:, 1:17);
        contour = [];
        for l = 1:(size(unstable_landmarks, 2)-1)
           x1 = unstable_landmarks(1, l);
           y1 = unstable_landmarks(2, l);
           x2 = unstable_landmarks(1, l+1);
           y2 = unstable_landmarks(2, l+1);
           [xs, ys] = bresenham(x1,y1,x2,y2);
           xy = [xs'; ys'];
           contour = [contour, xy];
        end

        % Prepare params
        width  = project.width;
        height = project.height;
        fW     = project.fW;
        fH     = project.fH;

        params    = struct();
        % Basic params goes here:
        if USE_ORTHO
            params.C  = [fW, 0., 0.;
                         0., fH, 0.;
                         0., 0., 0.];
        else
            params.C  = [fW, 0., width/2.;
                         0., fH, height/2.;
                         0., 0., 1.];
        end

        params.use_ortho         = USE_ORTHO;
        params.width             = width;
        params.height            = height;
        params.R                 = project.gtruth_rotation;
        params.t                 = project.gtruth_translation;

        params.alpha_dim         = project.alpha_dim;
        params.landmarks         = landmarks;
        params.landmarks_count   = size(landmarks, 2);
        params.static_indices    = static_indices;
        params.model             = MODEL;
        params.alpha_scale       = ALPHA_SCALE;
        params.translation_scale = TRANSLATION_SCALE;

        % Additional params for adaptive method
        params.contour           = contour;
        params.paths             = PATHS_CONTROL_POINTS;

        % Basic algorithm
        params.reg = 5;
%         reconstruction = basic(params);
        reconstruction = basic_jacob(params);

        % Adaptive algorithm
%         params.reg = 4;
%         reconstruction = adaptive(params);

        % Reprojection error
        error = 0.0;
        rec_shape = coef2object(reconstruction.alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);
        for j = 1:params.landmarks_count
           X      = rec_shape((3*params.static_indices(j) + 1):(3*params.static_indices(j) + 3));
           [x, y] = project_point(X, params.C, reconstruction.R, reconstruction.t, params.width, params.height, USE_ORTHO);
           proj  = [x; y];
           error  = error + norm(params.landmarks(:, j) - proj(1:2));
        end
        reprojection_avg_err = reprojection_avg_err + error / params.landmarks_count;

        % Shape error
        gtruth_model = load(TEST_FACES_PATHS{1, str2num(PROJECTS(i).name(strfind(PROJECTS(i).name, '.proj')-2)) + 1});
        gtruth_shape = gtruth_model.shape(:);
        n_vertex     = size(gtruth_shape, 1) / 3;
        shape_error  = 0.0;
        for j = 1:n_vertex
           e = norm(gtruth_shape((3*(j-1)+1):(3*(j-1)+3)) - rec_shape((3*(j-1)+1):(3*(j-1)+3)));
           shape_error = shape_error + e;
        end
        shape_avg_err = shape_avg_err + (shape_error / n_vertex) * TO_MM_FACTOR;
        
        render_and_save(gtruth_model.shape, gtruth_model.tex, MODEL.tl, 800, 600, strcat('./experiments/test_original_face', num2str(i),'.png'), USE_ORTHO);
        render_and_save(rec_shape, gtruth_model.tex, MODEL.tl, 800, 600, strcat('./experiments/test_angle', num2str(k), '_face', num2str(i), '.png'), USE_ORTHO);
    end
    
    alpha_avg_errors(k) = alpha_avg_err / N;
    reprojection_avg_errors(k) = reprojection_avg_err / N;
    shape_avg_errors(k) = shape_avg_err / N;
end

figure;

set(0,'DefaultAxesFontSize',14,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',14,'DefaultTextFontName','Times New Roman'); 

title('??????????? ???????? ????????????? ?? ????')
xlabel('???? (???????)')

yyaxis left;
plot(ANGLES, shape_avg_errors);
ylabel('?????? (??.)')

yyaxis right;
plot(ANGLES, reprojection_avg_errors);
ylabel('?????? ?????????????????? (????.)');

ax = gca;
ax.XTick = ANGLES;
5;