function [ SHAPE, TEXTURE, TL ] = gen_face(alpha)
%GEN_FACE Summary of this function goes here
%   Detailed explanation goes here

MODEL_PATH            = './bfm/MorphableModel.mat';

% Load 3DMM
model             = load(MODEL_PATH);

% shape_dim       = size(model.shapePC, 2);
% alpha           = zeros(shape_dim, 1);
beta              = zeros(size(model.texPC, 2), 1);

SHAPE             = coef2object(alpha, model.shapeMU, model.shapePC, model.shapeEV);
TEXTURE           = coef2object(beta,  model.texMU,   model.texPC,   model.texEV);
TL                = model.tl;

end

