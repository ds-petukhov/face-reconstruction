function [ ] = print2log(LOG, reprojection_error, alpha_error, shape_error, ...
                              rotation_error, translation_error, ...
                              smart_alpha_error, rmse_shape_error, ...
                              norm_angle_error)
    fprintf(LOG, '------------------------------------------------\n');

    fprintf(LOG, 'Mean RE: %f\n', mean(reprojection_error));
    fprintf(LOG, 'Median RE: %f\n', median(reprojection_error));
    fprintf(LOG, 'Std RE: %f\n', std(reprojection_error));

    fprintf(LOG, 'Mean AE: %f\n', mean(alpha_error));
    fprintf(LOG, 'Median AE: %f\n', median(alpha_error));
    fprintf(LOG, 'Std AE: %f\n', std(alpha_error));

    fprintf(LOG, 'Mean SE: %f\n', mean(shape_error));
    fprintf(LOG, 'Median SE: %f\n', median(shape_error));
    fprintf(LOG, 'Std SE: %f\n', std(shape_error));

    fprintf(LOG, '------------------------------------------------\n');

    fprintf(LOG, 'Mean RotE: %f\n', mean(rotation_error));
    fprintf(LOG, 'Median RotE: %f\n', median(rotation_error));
    fprintf(LOG, 'Std RotE: %f\n', std(rotation_error));

    fprintf(LOG, 'Mean TE: %f\n', mean(translation_error));
    fprintf(LOG, 'Median TE: %f\n', median(translation_error));
    fprintf(LOG, 'Std TE: %f\n', std(translation_error));

    fprintf(LOG, '------------------------------------------------\n');

    fprintf(LOG, 'Mean SmartAE: %f\n', mean(smart_alpha_error ));
    fprintf(LOG, 'Median SmartAE: %f\n', median(smart_alpha_error));
    fprintf(LOG, 'Std SmartAE: %f\n', std(smart_alpha_error));

    fprintf(LOG, 'Mean RMSE_SE: %f\n', mean(rmse_shape_error));
    fprintf(LOG, 'Median RMSE_SE: %f\n', median(rmse_shape_error));
    fprintf(LOG, 'Std RMSE_SE: %f\n', std(rmse_shape_error));

    fprintf(LOG, '------------------------------------------------\n');

    fprintf(LOG, 'Mean NormalsErr: %f\n', mean(norm_angle_error));
    fprintf(LOG, 'Median NormalsErr: %f\n', median(norm_angle_error));
    fprintf(LOG, 'Std NormalsErr: %f\n', std(norm_angle_error));
end

