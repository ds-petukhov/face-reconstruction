function reconstruction = adaptive(params)

    MODEL             = params.model;
    ALPHA_DIM         = params.alpha_dim;
    LANDMARKS_COUNT   = params.landmarks_count;
    STATIC_INDICES    = params.static_indices;
    LANDMARKS         = params.landmarks;
    ALPHA_SCALE       = params.alpha_scale;
    TRANSLATION_SCALE = params.translation_scale;
    LOCATION_SCALE    = params.location_scale;
    ROTATION_SCALE    = params.rotation_scale;
    C                 = params.C;

    CONTOUR           = params.contour;
    PATHS             = params.paths;
    N_PATHS           = size(PATHS, 2);

    USE_ORTHO         = params.use_ortho;
    WIDTH             = params.width;
    HEIGHT            = params.height;
    TRUE_R            = params.R;
    TRUE_t            = params.t;

    ITER_NUM          = 0;

    CUR_SPLINES       = struct();
%%%%%%%%%%%%%%%%%%%%% Compute distance transform %%%%%%%%%%%%%%%%%%%%%%%%%%
    FULL_CONTOUR       = zeros(HEIGHT, WIDTH);
    for i = 1:size(CONTOUR, 2)
        FULL_CONTOUR(CONTOUR(2, i), CONTOUR(1, i)) = 1;
    end
    [DISTANCE_MAP, ~] = bwdist(FULL_CONTOUR, 'euclidean');

%%%%%%%%%%%%%%%%%%%%%%%%% Make starting point %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    alpha0            = zeros(ALPHA_DIM, 1);
%     alpha0            = randn(ALPHA_DIM, 1);
    shape0            = coef2object(alpha0, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);

    if USE_ORTHO
        R0 = [1 0 0; 0 -1 0; 0 0 -1];
        t0 = [0; 0; 0];
    else
        opnp_3dpts = zeros(3, LANDMARKS_COUNT);
        for i = 1:size(STATIC_INDICES)
            opnp_3dpts(:, i) = shape0((3*STATIC_INDICES(i) + 1):(3*STATIC_INDICES(i) + 3));
        end

        opnp_2dpts = inv(C) * [LANDMARKS; ones(1, LANDMARKS_COUNT)];
        opnp_2dpts = opnp_2dpts(1:2, :);

        [R0, t0]    = OPnP(opnp_3dpts / 1e4, opnp_2dpts);
        t0          = t0 * 1e4;
    end

    rot_vector0 = rodrigues(R0);

    CUR_SPLINES = comp_splines(alpha0, R0, t0, shape0, PATHS);

    p0 = zeros(N_PATHS, 1);
    for i = 1:N_PATHS
%         p0(i) = rand();
        p0(i) = 0.5; % start at the middle
%         p0(i) = fminbnd(@(x) calc_angle(x, CUR_SPLINES.splines(i, :))^2, 0, 1);
    end

    x0 = [p0' / LOCATION_SCALE, alpha0' / ALPHA_SCALE, rot_vector0' / ROTATION_SCALE, t0' / TRANSLATION_SCALE];

    LOWER_BOUND = [zeros(1, N_PATHS) / LOCATION_SCALE, repmat(-Inf, 1, size(x0, 2) - N_PATHS)];
    UPPER_BOUND = [ones(1, N_PATHS) / LOCATION_SCALE, repmat(+Inf, 1, size(x0, 2) - N_PATHS)];

%%%%%%%%%%%%%%%%%%%%%%%%%%% Call lsqnonlin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% , 'OutputFcn',        @newiteration         ...
    problem.x0        = x0;
    problem.lb        = LOWER_BOUND;
    problem.ub        = UPPER_BOUND;
    problem.objective = @calc_energy;
    problem.options   = optimset( ...
                        'Algorithm',        'levenberg-marquardt' ...
                        'Jacobian',         'off'                 ...
                      , 'Display',          'iter'                ...
                      , 'TolFun',            1e-42                ...
                      , 'TolX',              1e-42                ...
                      , 'MaxIter',           20                   ...
                      , 'UseParallel',       true                 ...
    );
    problem.solver    = 'lsqnonlin';
    result            = lsqnonlin(problem);

%%%%%%%%%%%%%%%%%%%%%%%%%%% Return solution %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    reconstruction.alpha = result(N_PATHS + 1 : N_PATHS + ALPHA_DIM)' * ALPHA_SCALE;
    reconstruction.R     = rodrigues(result(N_PATHS + ALPHA_DIM + 1 :  N_PATHS + ALPHA_DIM + 3)' * ROTATION_SCALE);
    reconstruction.t     = result(end - 2 : end)' * TRANSLATION_SCALE;
    res_p                = result(1:N_PATHS)' * LOCATION_SCALE;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% Energy function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function energy = calc_energy(point)
        energy = zeros(1, 2*LANDMARKS_COUNT + 2*N_PATHS + ALPHA_DIM);

        cur_p          = point(1:N_PATHS)' * LOCATION_SCALE;
        cur_alpha      = point(N_PATHS + 1 : N_PATHS + ALPHA_DIM)' * ALPHA_SCALE;
        cur_rot_vector = point(N_PATHS + ALPHA_DIM + 1 :  N_PATHS + ALPHA_DIM + 3)' * ROTATION_SCALE;
        cur_t          = point(end - 2 : end)' * TRANSLATION_SCALE;
        cur_R          = rodrigues(cur_rot_vector);
        cur_shape      = coef2object(cur_alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);

        % Project face model using R, t & compute errors
        for i = 1:LANDMARKS_COUNT
            shape_pnt = cur_shape((3*STATIC_INDICES(i) + 1):(3*STATIC_INDICES(i) + 3));
            [x, y] = project_point(shape_pnt, C, WIDTH, HEIGHT, USE_ORTHO, cur_R, cur_t);
            energy(1, 2*i - 1) = (x - LANDMARKS(1, i)) * params.land_scale;
            energy(1, 2*i    ) = (y - LANDMARKS(2, i)) * params.land_scale;
        end

        if ~isequal(CUR_SPLINES.alpha, cur_alpha) || ~isequal(CUR_SPLINES.R, cur_R) || ~isequal(CUR_SPLINES.t, cur_t)
            CUR_SPLINES = comp_splines(cur_alpha, cur_R, cur_t, cur_shape, PATHS);
        end

        for i = 1:N_PATHS
            cur_pi = cur_p(i);
            angle = calc_angle(cur_pi, CUR_SPLINES.splines(i, :));
            energy(1, i + 2*LANDMARKS_COUNT) = angle * params.angle_scale;

            spline_pnt = eval_point(CUR_SPLINES.splines(i, :), cur_pi);

            [x, y] = project_point(spline_pnt, C, WIDTH, HEIGHT, USE_ORTHO);
            proj = [x; y];
            dist = my_distance2curve(proj);
            energy(1, i + 2*LANDMARKS_COUNT + N_PATHS) = dist * params.dist_scale;
        end

        energy(1, 1 + 2*LANDMARKS_COUNT + 2*N_PATHS : end) = params.reg .* (MODEL.shapeEV(1:ALPHA_DIM) .* cur_alpha);
    end

    function angle = calc_angle(p, splines)
        N = eval_normal(splines, p);
        N = N / norm(N);
        angle = dot(N, [0, 0, 1]);
    end

    function dist = my_distance2curve(point)
        y = max(1, min(point(2), HEIGHT));
        x = max(1, min(point(1), WIDTH));
        dist = interp2(1:WIDTH, 1:HEIGHT, DISTANCE_MAP, x, y, 'cubic');
    end

%% HELPERS
    function normal = eval_normal(path_splines, p)
        nx_spline = path_splines(4);
        ny_spline = path_splines(5);
        nz_spline = path_splines(6);

        normal = [fnval(nx_spline, p); fnval(ny_spline, p); fnval(nz_spline, p)];
    end

    function point = eval_point(path_splines, p)
        x_spline = path_splines(1);
        y_spline = path_splines(2);
        z_spline = path_splines(3);

        point = [fnval(x_spline, p); fnval(y_spline, p); fnval(z_spline, p)];
    end
end
