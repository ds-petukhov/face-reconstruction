#!/usr/bin/python
import matlab.engine
import os

from subprocess import call

from render_face import Renderer
from PIL import Image, ImageDraw
from scipy import misc, ndimage
import numpy as np
import sys
import time

import dlib
from skimage import io
from skimage.draw import circle

WIDTH = 1200;
HEIGHT = 900;
ANGLES = [-30, -20, -10, 0, 10, 20, 30]

MATLAB_PROJECT_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code"

LAND_COUNT = 68
PREDICTOR_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code/extras/shape_predictor_68_face_landmarks.dat"
DETECTOR = dlib.get_frontal_face_detector()
PREDICTOR = dlib.shape_predictor(PREDICTOR_PATH)

def landmarks2str(landmarks):
    landmarks_str = ""
    for i in xrange(0, LAND_COUNT):
        p = landmarks.part(i)
        landmarks_str = landmarks_str + str(p.x) + ":" + str(p.y) + ","
    return landmarks_str[:-1]

def mk_project_file(img_path, landmarks, landmarks_count, alpha_dim, alpha, focal_length, R, t):
    with open(img_path[:-4] + ".proj", 'w') as f:
        f.write("proj\n")
        f.write("format ascii 1.0\n")
        f.write("image:\n")
        f.write(img_path + '\n')
        f.write("resolution:\n")
        f.write("{},{}".format(WIDTH, HEIGHT) + '\n')
        f.write("focal_length:\n")
        f.write("{},{}\n".format(focal_length[0], focal_length[1]))
        f.write("landmarks_count:\n")
        f.write(str(landmarks_count) + '\n')
        f.write("landmarks:\n")
        f.write(str(landmarks) + '\n')
        f.write("alpha_dim:\n")
        f.write(str(alpha_dim) + '\n')
        f.write("true_alpha:\n")
        f.write(str(alpha) + '\n')
        f.write("true_rotation:\n")
        f.write("{},{},{}\n".format(R[0][0], R[0][1], R[0][2]))
        f.write("{},{},{}\n".format(R[1][0], R[1][1], R[1][2]))
        f.write("{},{},{}\n".format(R[2][0], R[2][1], R[2][2]))
        f.write("true_translation:\n")
        f.write("{},{},{}\n".format(t[0], t[1], t[2]))

def main():
    print "Initializing matlab..."
    eng = matlab.engine.start_matlab()
    eng.addpath(eng.genpath(MATLAB_PROJECT_PATH))

    for path, dirs, files in os.walk('/Users/dmitrypetuhov/Desktop/diploma/code/bfm_test_scans'):
        for i, file in enumerate(files):
            alpha, shape, tex, tl = eng.return_test_face(os.path.join(path, file), nargout=4)
            alpha, shape, tex, tl = alpha[0], shape[0], tex, tl

            for j, angle in enumerate(ANGLES):
                renderer = Renderer(WIDTH, HEIGHT, minAngle=angle, maxAngle=angle)
                renderer.set_texture(tex)
                renderer.set_tl(tl)
                image = renderer.render_shape(shape)
                img_path = "/Users/dmitrypetuhov/Desktop/diploma/code/experiments/face"+str(i)+ str(j) + ".png"
                R = renderer.get_rotation_matrix()
                t = renderer.get_translation_vector()
                f = renderer.get_focal_length()
                image.save(img_path)

                alpha_dim = len(alpha)
                print "alpha dim =", alpha_dim
                print alpha

                img = io.imread(img_path)

                print "Detecting landmarks...",
                dets = DETECTOR(img, 1)
                landmarks = PREDICTOR(img, dets[0])
                print "OK!"

                print "Draw landmarks on the image...",
                for j in xrange(0, LAND_COUNT):
                    p = landmarks.part(j)
                    rr, cc = circle(p.y, p.x, 2)
                    img[rr, cc] = [0, 255, 0]
                misc.imsave(img_path, img)
                print "OK!"

                print "Making proj file...",
                landmarks_str = landmarks2str(landmarks)
                alpha_str = str(alpha)[1:-1] # drop '[' and ']'
                mk_project_file(img_path, landmarks_str, LAND_COUNT, alpha_dim, alpha_str, f, R, t)
                print "OK!"

if __name__ == "__main__":
    main()
