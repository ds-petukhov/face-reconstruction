#!/usr/bin/python
import matlab.engine
import os

from subprocess import call

from render_face import Renderer
from PIL import Image
from render_face import Renderer
from PIL import Image, ImageDraw
from scipy import misc, ndimage
import numpy as np
import dlib
from skimage import io
from skimage.draw import circle

MATLAB_PROJECT_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code"
FACE_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code/experiments/path_face.png"
PATH_COLOR = [255, 0, 0, 255]
WIDTH = 1200;
HEIGHT = 900;

LAND_COUNT = 68
PREDICTOR_PATH = "/Users/dmitrypetuhov/Desktop/diploma/code/extras/shape_predictor_68_face_landmarks.dat"
DETECTOR = dlib.get_frontal_face_detector()
PREDICTOR = dlib.shape_predictor(PREDICTOR_PATH)

def parse_landmarks(landmarks_path):
    with open(landmarks_path) as f:
        landmarks = [line.split() for line in f if line.strip() and len(line.split()) == 2]
        count = landmarks[0][1]
        landmarks = map(lambda l: (float(l[0]), float(l[1])), landmarks[1:])
        return count, landmarks

def main():
    print "Initializing matlab...",
    eng = matlab.engine.start_matlab()
    eng.addpath(eng.genpath(MATLAB_PROJECT_PATH))
    print "OK!"

    print "Generate average face...",
    alpha0 = eng.zeros(1, 1)
    shape, tex, tl = eng.gen_face(alpha0, nargout=3)
    print "OK!"

    print "Init renderer...",
    renderer = Renderer(WIDTH, HEIGHT, minAngle=0, maxAngle=0, useOrtho=True)
    renderer.set_texture(tex)
    renderer.set_tl(tl)
    print "OK!"

    print "Rendering...",
    image = renderer.render_shape(shape)
    print "OK!"

    print "Saving the image...",
    image.save(FACE_PATH)
    print "OK!"

    done = False
    while not done:
        print "Enter either 'p' to process an image with drawn path on it or 'q' to quit:"
        command = str(raw_input())

        if command == 'p':
            print "Path to the image (png):"
            path = str(raw_input()).strip()
            try:
                img = misc.imread(path)
                indices = np.where(np.all(img == PATH_COLOR, axis=-1))
                coords = zip(indices[0], indices[1])
                # N = len(coords) / 3
                # points = coords[::len(coords) / (N-2)] + [coords[-1]]
                points = coords
                points.sort(key=lambda (y, x): (y, -x)) # up -> down, right -> left
                # Process path
                print "[DEBUG]: Number of points = ", len(points), "of total", len(coords), "{0}".format(len(points)/float(len(coords)))
                img_marked = img.copy()
                result_path = path[:-4] + ".txt"
                with open(result_path, 'w') as f:
                    for i, (y, x) in enumerate(points):
                        if i == 0:
                            img_marked[y, x] = [0, 0, 0, 255]
                        else:
                            img_marked[y, x] = [255, 255, 255, 255]
                        bary, indices, _ = renderer.find_triangle_for(x, y) # make it x, y
                        print '---' * 10
                        print "For (", x, y, ") result: ", bary, indices
                        f.write("{0}, {1}, {2}, {alpha}, {beta}, {gamma}\n".format(*indices, **bary))
                        # print "Result saved"
                        print '---' * 10
                misc.imsave(path[:-4] + "_{0}_of_{1}_done.png".format(len(points), len(coords)), img_marked)

            except IndexError:
                print "No path on the image"
            except IOError:
                print "No such file"

        elif command == 'd':
            print "Path to the path folder (.txt):"
            path = str(raw_input()).strip()
            image = np.array(image)
            for dir, _, files in os.walk(path):
                for file_name in files:
                    if file_name.endswith('.txt'):
                        file_path = os.path.join(dir, file_name)
                        with open(file_path) as f:
                            for line in f:
                                point = map(lambda s: float(s.strip()), line.split(","))
                                x, y = renderer.project_bary_point(point[0], point[1], point[2], point[3], point[4], point[5])
                                image[y, x] = [0, 0, 255]
            image = Image.fromarray(image)
            image.save(FACE_PATH)

        elif command == 'q':
            print "Bye-bye!"
            done = True

if __name__ == "__main__":
    main()
