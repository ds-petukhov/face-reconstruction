function [ alpha shape tex tl ] = return_test_face( path )
%RETURN_TEST_FACE Summary of this function goes here
%   Detailed explanation goes here
    
    [model msz] = load_model();
    shape_dim  = size(model.shapePC, 2);
    test_face = load(path);
    
    alpha = zeros(1, shape_dim);
    shape = test_face.shape(:)';
    tex = test_face.tex(:);
    tl = model.tl;
end

