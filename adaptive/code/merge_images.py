import sys
from PIL import Image

image_names = [ "iter1.png",
                "iter2.png",
                "iter3.png",
                "iter4.png",
                "iter5.png",
                "iter6.png",
                "iter7.png",
                "iter8.png",
                "iter9.png",
                "iter10.png",
                "iter11.png",
                "face1.png" ]

images = map(Image.open, image_names)
widths, heights = zip(*(i.size for i in images))

total_width = sum(widths)
max_height = max(heights)

new_im = Image.new('RGB', (total_width, max_height))

x_offset = 0
for im in images:
  new_im.paste(im, (x_offset,0))
  x_offset += im.size[0]

new_im.save('test.png')
