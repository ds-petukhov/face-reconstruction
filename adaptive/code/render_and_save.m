function [ ] = render_and_save(shp, tex, tl, width, height, path, USE_ORTHO)

    shp = reshape(shp, [ 3 prod(size(shp))/3 ])'; 
    tex = reshape(tex, [ 3 prod(size(tex))/3 ])'; 
    tex = min(tex, 255);
    
    FOV = 45; % Really?
    
    set(gcf, 'Renderer', 'opengl');
    set(gcf, 'Units', 'pixels');
    set(gcf, 'PaperUnits','inches','PaperPosition',[0 0 width/100 height/100])
    camva(FOV);  % Set the camera field of view
    
    fig_pos = get(gcf, 'Position');
    fig_pos(1) = 1;
    fig_pos(2) = 1;
    fig_pos(3) = width;
    fig_pos(4) = height;
    set(gcf, 'Position', fig_pos);

    mesh_h = trimesh(...
        tl, shp(:, 1), shp(:, 3), shp(:, 2), ...
        'EdgeColor', 'none', ...
        'FaceVertexCData', tex/255, ...
        'FaceColor', 'interp' ...
    );
    
    if USE_ORTHO
        set(gca, ...
            'DataAspectRatio', [ 1 1 1 ], ...
            'PlotBoxAspectRatio', [ 1 1 1 ], ...
            'Units', 'pixels', ...
            'GridLineStyle', 'none', ...
            'Position', [ 0 0 fig_pos(3) fig_pos(4) ], ...
            'Visible', 'off', 'box', 'off', ...
            'Projection', 'orthographic' ...
            );
    else
        set(gca, ...
            'DataAspectRatio', [ 1 1 1 ], ...
            'PlotBoxAspectRatio', [ 1 1 1 ], ...
            'Units', 'pixels', ...
            'GridLineStyle', 'none', ...
            'Position', [ 0 0 fig_pos(3) fig_pos(4) ], ...
            'Visible', 'off', 'box', 'off', ...
            'Projection', 'perspective' ...
            );
    end
    
    set(gcf, 'Color', [ 0 0 0 ]); 
    view(180, 0);
    

    material([.5, .5, .1 1  ]);
    camlight('headlight');

    set(gcf, 'visible', 'off');
    
%     set(gcf,'PaperpositionMode','Auto');
    saveas(gcf, path);
%     print(path, '-dpng', '-r100')
end

