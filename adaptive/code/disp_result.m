function [  ] = disp_result( rec_alpha )
%DISP_RESULT Summary of this function goes here
%   Detailed explanation goes here
    [model msz] = load_model();

    beta  = zeros(msz.n_tex_dim, 1);
    shape  = coef2object( rec_alpha, model.shapeMU, model.shapePC, model.shapeEV );
    tex    = coef2object( beta,  model.texMU,   model.texPC,   model.texEV );

    % Render it
    rp     = defrp;
    rp.phi = 0.0;
    rp.dir_light.dir = [0;0;0];
    rp.dir_light.intens = 0.0*ones(3,1);
    display_face(shape, tex, model.tl, rp);
end

