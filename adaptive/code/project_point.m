function [ x, y ] = project_point( X, C, WIDTH, HEIGHT, ORTHO, R, t)
    if nargin == 5 % no R, t was in the input
        proj      = C*X;
    else
        proj      = C*(R*X + t);
    end
    if ORTHO
        x         = proj(1) + WIDTH / 2;
        y         = proj(2) + HEIGHT / 2;
    else
        proj      = proj ./ proj(3);
        x         = proj(1);
        y         = proj(2);
    end
end

