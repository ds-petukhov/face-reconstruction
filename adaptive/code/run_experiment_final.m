% 1. Run mk_experiment.py
% 2. Run get_static_indices.py
% 3. Run this script
clear;
EXPERIMENTS_PATH      = './experiments/*.proj';
MODEL_PATH            = './bfm/MorphableModel.mat';
INDICES_PATH          = './experiments/static_indecies.txt';
PATHS_PATH            = './paths/*.txt';

PATHS                 = dir(PATHS_PATH);

PROJECTS              = dir(EXPERIMENTS_PATH);

MODEL                 = load(MODEL_PATH);

ALPHA_SCALE           = 1e8;
TRANSLATION_SCALE     = 1e8;
ROTATION_SCALE        = 1e8;
LOCATION_SCALE        = 1e8;

N                     = size(PROJECTS, 1);

USE_ORTHO             = true;

ANGLES = [-30, -20, -10, 0, 10, 20, 30];
ANGLES_CNT = size(ANGLES, 2);
ANGLE2INX = zeros(ANGLES_CNT, 1);

RESULTS = struct(...
    'alpha_error',        zeros(N, 1),  ...
    'smart_alpha_error',  zeros(N, 1),  ...
    'reprojection_error', zeros(N, 1),  ...
    'shape_error',        zeros(N, 1),  ...
    'rmse_shape_error',   zeros(N, 1),  ...
    'norm_angle_error',   zeros(N, 1),  ...
    ...
    'rotation_error',     zeros(N, 1),  ...
    'translation_error',  zeros(N, 1),  ...
    'scale_error',        zeros(N, 1)   ...
);

LOG = fopen('./experiments/log_adaptive.txt', 'w');
fprintf(LOG,'truth \t\t reconstructed \n');

% Read path's control points
PATHS_CONTROL_POINTS = [];
for k=1:size(PATHS, 1)
    PATHS_CONTROL_POINTS = [PATHS_CONTROL_POINTS, read_path_file(PATHS(k).name)];
end

TEST_TEXTURE = coef2object(zeros(size(MODEL.texPC, 2), 1),  MODEL.texMU,   MODEL.texPC,   MODEL.texEV);

for i=1:N
    display(i);
    display(PROJECTS(i).name);
    project = read_project(PROJECTS(i).name);

    % Choose landmarks & indices
    indices = read_indecies(INDICES_PATH);
    static_indices = indices;
    landmarks = project.landmarks(:, 18:end);
    assert(size(static_indices, 1) == size(landmarks, 2));

    % Make a contour from unstable landmarks
    unstable_landmarks = project.landmarks(:, 1:17);
    contour = [];
    for l = 1:(size(unstable_landmarks, 2)-1)
       x1 = unstable_landmarks(1, l);
       y1 = unstable_landmarks(2, l);
       x2 = unstable_landmarks(1, l+1);
       y2 = unstable_landmarks(2, l+1);
       [xs, ys] = bresenham(x1,y1,x2,y2);
       xy = [xs'; ys'];
       contour = [contour, xy];
    end

    % Prepare params
    width  = project.width;
    height = project.height;
    fW     = project.fW;
    fH     = project.fH;

    params    = struct();
    % Basic params goes here:
    if USE_ORTHO
        params.C  = [fW, 0., 0.;
                     0., fH, 0.;
                     0., 0., 0.];
    else
        params.C  = [fW, 0., width/2.;
                     0., fH, height/2.;
                     0., 0., 1.];
    end

    params.use_ortho         = USE_ORTHO;
    params.width             = width;
    params.height            = height;
    params.R                 = project.gtruth_rotation;
    params.t                 = project.gtruth_translation;

    params.alpha_dim         = project.alpha_dim;
    params.landmarks         = landmarks;
    params.landmarks_count   = size(landmarks, 2);
    params.static_indices    = static_indices;
    params.model             = MODEL;
    params.alpha_scale       = ALPHA_SCALE;
    params.translation_scale = TRANSLATION_SCALE;
    params.location_scale    = LOCATION_SCALE;
    params.rotation_scale    = ROTATION_SCALE;

    % Additional params for adaptive method
    params.contour           = contour;
    params.paths             = PATHS_CONTROL_POINTS;

%     % Basic algorithm
    params.reg = 1e-3;
    reconstruction = basic(params);

    % Adaptive algorithm
%     params.reg = 1e-3;
%     params.land_scale = 100;
%     params.angle_scale = 50;
%     params.dist_scale = 10;
%     reconstruction = adaptive(params);

    % Alpha error
    RESULTS.alpha_error(i) = sum(abs((reconstruction.alpha - project.gtruth_alpha)));
    RESULTS.smart_alpha_error(i) = sum( ( MODEL.shapeEV(1:params.alpha_dim) .* (reconstruction.alpha - project.gtruth_alpha) ) .^2 );

    % Reprojection error
    error = 0.0;
    rec_shape = coef2object(reconstruction.alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);
    for j = 1:params.landmarks_count
       X = rec_shape((3*params.static_indices(j) + 1):(3*params.static_indices(j) + 3));
       [x, y] = project_point(X, params.C, params.width, params.height, USE_ORTHO, reconstruction.R, reconstruction.t);
       proj = [x; y];
       error = error + norm(params.landmarks(:, j) - proj(1:2));
    end
    RESULTS.reprojection_error(i) = error / params.landmarks_count;

    % Shape error
    gtruth_shape = coef2object(project.gtruth_alpha, MODEL.shapeMU, MODEL.shapePC, MODEL.shapeEV);
    n_vertex     = size(gtruth_shape, 1) / 3;
    shape_error  = 0.0;
    sum_rmse = 0.0;
    for j = 1:n_vertex
        vert_truth = gtruth_shape((3*(j-1)+1):(3*(j-1)+3));
        vert_rec = rec_shape((3*(j-1)+1):(3*(j-1)+3));
        e = norm(vert_truth - vert_rec);
        shape_error = shape_error + e;
        sum_rmse = sum_rmse + e^2;
    end
    RESULTS.shape_error(i) = shape_error / n_vertex;
    RESULTS.rmse_shape_error(i) = sqrt(sum_rmse / n_vertex);

    % Normal error
    norm_angles_sum = 0;
    for j = 1:size(MODEL.tl, 1)
        id1 = MODEL.tl(j, 1);
        id2 = MODEL.tl(j, 2);
        id3 = MODEL.tl(j, 3);

        A_truth = gtruth_shape(3*(id1 - 1) + 1 : 3*(id1 - 1) + 3);
        B_truth = gtruth_shape(3*(id2 - 1) + 1 : 3*(id2 - 1) + 3);
        C_truth = gtruth_shape(3*(id3 - 1) + 1 : 3*(id3 - 1) + 3);

        A_rec = rec_shape(3*(id1 - 1) + 1 : 3*(id1 - 1) + 3);
        B_rec = rec_shape(3*(id2 - 1) + 1 : 3*(id2 - 1) + 3);
        C_rec = rec_shape(3*(id3 - 1) + 1 : 3*(id3 - 1) + 3);

        normal_truth = cross(B_truth - A_truth, C_truth - A_truth);
        normal_truth = normal_truth / norm(normal_truth);

        normal_rec = cross(B_rec - A_rec, C_rec - A_rec);
        normal_rec = normal_rec / norm(normal_rec);

        norm_angles_sum = norm_angles_sum + atan2(norm(cross(normal_truth, normal_rec)), dot(normal_truth, normal_rec)) * 180 / pi;
    end
    RESULTS.norm_angle_error(i) = norm_angles_sum / size(MODEL.tl, 1);

    % Rotation error
    R_true = project.gtruth_rotation;
    R_rec  = reconstruction.R;
    rot_err = acos(dot(R_true(:, 1), R_rec(:, 1))) * 180 / pi;
    rot_err = max(rot_err, acos(dot(R_true(:, 2), R_rec(:, 2))) * 180 / pi);
    RESULTS.rotation_error(i) = rot_err;

    % Translation error : e = ||t_true - t||
    RESULTS.translation_error(i) = norm(project.gtruth_translation(1:2) - reconstruction.t(1:2));

    % Determine angle and write errors to the appropriate cell
    cur_angle = str2num(PROJECTS(i).name( (strfind(PROJECTS(i).name, '_') + 1) : (strfind(PROJECTS(i).name, '.proj') - 1) ));
    angle_idx = find(ANGLES == cur_angle);
    ANGLE2INX(angle_idx, end + 1) = i;

    % Print to LOG
    for j=1:params.alpha_dim
       fprintf(LOG, '%f', project.gtruth_alpha(j,:));
       fprintf(LOG, '\t--\t');
       fprintf(LOG, '%f', reconstruction.alpha(j,:));
       fprintf(LOG, '\n');
    end
end

print2log(LOG, RESULTS.reprojection_error, RESULTS.alpha_error, RESULTS.shape_error, ...
               RESULTS.rotation_error, RESULTS.translation_error, ...
               RESULTS.smart_alpha_error, RESULTS.rmse_shape_error, ...
               RESULTS.norm_angle_error);

for ai = 1:ANGLES_CNT
    fprintf(LOG, '------------------------------------------------\n');
    fprintf(LOG, strcat('--------->  ', num2str(ANGLES(ai)), ' <----------\n' ));
    inx = ANGLE2INX(ai, ANGLE2INX(ai, :) ~= 0);
    print2log(LOG, RESULTS.reprojection_error(inx), RESULTS.alpha_error(inx), RESULTS.shape_error(inx), ...
                   RESULTS.rotation_error(inx), RESULTS.translation_error(inx), ...
                   RESULTS.smart_alpha_error(inx), RESULTS.rmse_shape_error(inx), ...
                   RESULTS.norm_angle_error(inx));
end

save('./experiments/exp_data_adaptive.mat', 'RESULTS', 'ANGLE2INX');

fclose(LOG);
