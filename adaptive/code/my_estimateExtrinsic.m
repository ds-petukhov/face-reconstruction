function [ K, R, t ] = my_estimateExtrinsic(x, X)
%     [ x1 ... xn ]
% x = [ y1 ... yn ] size(x) = (2, n)
% 
%     [ X1 ... Xn ]
%     [ Y1 ... Yn ]
% X = [ Z1 ... Zn ] size(X) = (3, n)
% 
    x_orig = x;
    X_orig = X;
    
    %% Normalization
    x_dim   = size(x, 1);
    npoints = size(x, 2);
    
    %% Make sure centroid is in origin
    x_centroid = mean(x(1:2,:)')';
    X_centroid = mean(X(1:3,:)')';
    
    x(1,:) = x(1,:) - x_centroid(1);
    x(2,:) = x(2,:) - x_centroid(2);
    
    X(1,:) = X(1,:) - X_centroid(1);
    X(2,:) = X(2,:) - X_centroid(2);
    X(3,:) = X(3,:) - X_centroid(3);
    
   %% Normalize the points so that the average distance from the origin is equal to sqrt(2).
    x_averagedist = mean(sqrt(x(1,:).^2 + x(2,:).^2));
    X_averagedist = mean(sqrt(X(1,:).^2 + X(2,:).^2 + X(3,:).^2));

    x_scale = sqrt(2) / x_averagedist;
    X_scale = sqrt(3) / X_averagedist;

    x(1:2,:) = x_scale * x(1:2,:);
    X(1:3,:) = X_scale * X(1:3,:);
    
    %% similarity transform T
    T = [x_scale        0  -x_scale*x_centroid(1)
           0      x_scale  -x_scale*x_centroid(2)
           0            0                      1];

    %% similarity transform U
    U = [X_scale       0       0   -X_scale*X_centroid(1)
           0     X_scale       0   -X_scale*X_centroid(2)
           0           0  X_scale  -X_scale*X_centroid(3)
           0           0       0                       1];

    %% Compute the Camera Perpective Projection Matrix
% A = [X(1,:)'            X(2,:)'             X(3,:)'             ones(npoints,1) ...
%      zeros(npoints,1)    zeros(npoints,1)     zeros(npoints,1)     zeros(npoints,1) ...
%      -x(1,:)'.*X(1,:)'  -x(1,:)'.*X(2,:)'   -x(1,:)'.*X(3,:)'   -x(1,:)';
%      
%      zeros(npoints,1)    zeros(npoints,1)     zeros(npoints,1)     zeros(npoints,1) ...
%      X(1,:)'            X(2,:)'             X(3,:)'             ones(npoints,1) ...
%      -x(2,:)'.*X(1,:)'  -x(2,:)'.*X(2,:)'   -x(2,:)'.*X(3,:)'   -x(2,:)' ];
%  
%     [~, ~, V] = svd(A);
%     P = reshape(V(:,12),4,3)';

    %% Compute the Camera Orthographic Projection Matrix
    A = [X(1,:)'            X(2,:)'             X(3,:)'             ones(npoints,1) ...
        zeros(npoints,1)    zeros(npoints,1)     zeros(npoints,1)     zeros(npoints,1);
     
        zeros(npoints,1)    zeros(npoints,1)     zeros(npoints,1)     zeros(npoints,1) ...
        X(1,:)'            X(2,:)'             X(3,:)'             ones(npoints,1)];

    b = [x(1, :)'; x(2, :)']; 
    
    P = reshape(pinv(A) * b, 4, 2)';
    P(3, :) = [0, 0, 0, 1];
    
    %% Denormalization
    P = inv(T)*P*U;
    
    %% ---------------------------------------
    %% ---------- P decomposition ------------
    %% ---------------------------------------
    
    %% Normalized Projection Matrix
%     scale = sqrt(P(3,1)^2 + P(3,2)^2 + P(3,3)^2);
%     P = P / scale;

    %% Principal Point
    K(1,3) = P(1,1:3)*P(3,1:3)';
    K(2,3) = P(2,1:3)*P(3,1:3)';

    %% Focal Length
    K(1,1)   = sqrt(P(1,1:3)*P(1,1:3)' - K(1,3)^2);
    K(2,2)   = sqrt(P(2,1:3)*P(2,1:3)' - K(2,3)^2);
    K(3,1:3) = [0 0 1];

    %% Decompose P into R, t
    scale = mean([1 / norm(P(:,1)), 1 / norm(P(:,2))]);
    t     = P(:,3)*scale;
    r1    = P(:,1)*scale;
    r2    = P(:,2)*scale;
    r3    = cross(r1,r2);
    R     = [r1 r2 r3];

    %% Orthogonal constraint
    [U S V] = svd(R);
    R = U*V.';

    if det(R) < 0
        R = [R(:,1) R(:,2) -R(:,3)];
    end
    
    f = (K(1,1)+K(2,2)) / 2;
    
    %% Attemp
    x_img = x_orig(:, 1:3)';
    x_img(:, 1) = x_img(:, 1) - 600;
    x_img(:, 2) = x_img(:, 2) - 450;
    B = inv(K) * [x_img ones(size(x_img, 1), 1)]';
    B = B';
    
    A = X_orig(:, 1:3)';
    
    M = A' * B;
    
    [U, S, V] = svd(M);
    
    S_ = S;
    S_(3, 3) = sign(det(U*V'));
    S_(2, 2) = 1;
    S_(1, 1) = 1;
    
    R_ = U*S_*V';
end

