function [ points ] = read_3dpts(fname)
%READ_3DPTS Summary of this function goes here
%   Detailed explanation goes here
    file = fopen(fname, 'r');
    line = fgets(file);
    if ~isempty(strfind(line, 'n_points'))
        filedata = textscan(file, '%f %f %f', 'delimiter', ' ', 'HeaderLines', 1);
        [x, y, z] = filedata{:};
    end
    points = [x, y, z]';
    fclose(file);
end

