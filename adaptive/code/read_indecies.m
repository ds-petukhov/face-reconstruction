function [ indecies ] = read_indecies( fname )
    file = fopen(fname, 'r');
    filedata = textscan(file, '%d', 'delimiter', ' ', 'HeaderLines', 0);
    indecies = filedata{:};
%     indecies = indecies - 1;
    fclose(file);
end