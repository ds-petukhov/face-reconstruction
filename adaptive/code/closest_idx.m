function [idx] = closest_idx(face, point)
%CLOSEST_IDX Summary of this function goes here
%   Detailed explanation goes here
    n_pts = size(face, 1) / 3;
    best_idx = 0;
    best_error = norm(face(1:3) - point);
    for i = 1:(n_pts-1)
        error = norm(face((3*i + 1):(3*i + 3)) - point);
        if error < best_error
            best_error = error;
            best_idx = i;
    end
    idx = best_idx;
end

