function [ ALPHAS, SHAPES, TEXTURE, TL ] = rand_shape(ALPHA_MEAN, ALPHA_VAR, N)

ALPHA_DEVIATION = sqrt(ALPHA_VAR);
PATH_TO_MODEL = './bfm/MorphableModel.mat';

model = load(PATH_TO_MODEL);
shape_dim  = size(model.shapePC, 2);
% shape_dim  = 10;

ALPHAS = zeros(N, shape_dim);
SHAPES = zeros(N, 160470);

for i = 1:N
    fprintf('%i\n', i);
%     alpha = ALPHA_DEVIATION .* randn(shape_dim, 1) + ALPHA_MEAN;
    alpha = randn(shape_dim, 1);
%     alpha = zeros(shape_dim, 1);
    ALPHAS(i, :) = alpha;
    SHAPES(i, :) = coef2object(alpha, model.shapeMU, model.shapePC, model.shapeEV);
end

beta     = zeros(size(model.texPC, 2), 1);
TEXTURE  = coef2object(beta,  model.texMU,   model.texPC,   model.texEV);
TL       = model.tl;

end

