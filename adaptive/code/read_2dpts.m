function [ points ] = read_2dpts( fname )
%READ_2DPTS Summary of this function goes here
%   Detailed explanation goes here
    file = fopen(fname, 'r');
    line = fgets(file);
    if ~isempty(strfind(line, 'n_points'))
        filedata = textscan(file, '%f %f', 'delimiter', ' ', 'HeaderLines', 1);
        [x, y] = filedata{:};
    end
    points = [x, y]';
    fclose(file);
end

