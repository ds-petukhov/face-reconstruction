function [ points, normals ] = control_points_and_normals(shape, path, R, t)
%BARY_TO_EUCLID Summary of this function goes here
%   Detailed explanation goes here

    n_control_points = size(path.indices, 2);
    points = zeros(3, n_control_points);
    normals = zeros(3, n_control_points);
    for i = 1:n_control_points
        id1 = path.indices(1,i);
        id2 = path.indices(2,i);
        id3 = path.indices(3,i);
        
        A = shape(3*id1 + 1 : 3*id1 + 3);
        B = shape(3*id2 + 1 : 3*id2 + 3);
        C = shape(3*id3 + 1 : 3*id3 + 3);
        
        if nargin == 4
            A = R*A + t;
            B = R*B + t;
            C = R*C + t;
        end
        
        alpha = path.coords(1,i);
        beta  = path.coords(2,i);
        gamma = path.coords(3,i);
        
        point = alpha * A + beta * B + gamma * C;
        points(:, i) = point;
        
        normal        = cross(B-A,C-A);
        normal        = normal / norm(normal);
        normals(:, i) = normal;
        
%         dot(normal, B-A);
%         atan2d(norm(cross(normal,point)), dot(normal,point));
    end
end

