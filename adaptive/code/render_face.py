from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.arrays.vbo import VBO

import sys
import numpy as np

from PIL import Image
from math import atan, pi, tan
from random import uniform
from PIL import Image, ImageDraw
from scipy import misc, ndimage

class Renderer(object):
    def __init__(self, width, height, fieldOfView=45, minAngle=0, maxAngle=0, useOrtho=True, notML=True):
        self.notML = notML # if data will not come from MATLAB
        self.width = width
        self.height = height
        self.fieldOfView = 45
        self.aspectRatio = float(self.width) / float(self.height)
        self.minAngle = minAngle
        self.maxAngle = maxAngle
        self.T = np.array([[1,0,0], [0,-1,0], [0,0,-1]])
        self.useOrtho = useOrtho
        self.last_render = None

        glutInitWindowSize(self.width, self.height)
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH)
        glutInit()

        self.win = glutCreateWindow('Renderer')
        glutDisplayFunc(self.display_shape)
        glClearColor(255. / 255, 255. / 255, 255. / 255, 1.)

        glEnable(GL_DEPTH_TEST)
        # Accept fragment if it closer to the camera than the former one
        glDepthFunc(GL_LESS)
        # Cull triangles which normal is not towards the camera
        glEnable(GL_CULL_FACE)
        glEnable(GL_NORMALIZE)
        glFrontFace(GL_CW)
        glutGet(GLUT_ELAPSED_TIME)


    def __render(self):
        glutMainLoop()


    def set_texture(self, tex):
        if self.notML:
            self.texture = np.array(tex, dtype='f')
        else:
            self.texture = np.array(tex._data, dtype='f')
        vbo_tex = self.texture.reshape( (len(self.texture) / 3, 3) ) / 255.
        self.texture_vbo = VBO(vbo_tex)


    def set_tl(self, tl):
        if self.notML:
            self.tl = np.array(tl, dtype=np.int32)
        else:
            self.tl = np.array(tl._data, dtype=np.int32).reshape(3, -1).T
        self.tl = self.tl - 1 # omg, MATLAB starts counting from 1 !!! >_<'
        self.tl_vbo = VBO(self.tl, target=GL_ELEMENT_ARRAY_BUFFER)


    def __set_shape(self, shape):
        self.shape = np.array(shape, dtype='f')
        self.vertex_count = len(self.shape) / 3
        vbo_shape = self.shape.reshape( (len(self.shape) / 3, 3) )
        self.shape_vbo = VBO(vbo_shape)


    def get_focal_length(self):
        projection = glGetDoublev(GL_PROJECTION_MATRIX)
        fW = projection[0,0]
        fH = projection[1,1]
        return (fW * self.width / 2, fH * self.height / 2)


    def render_shape(self, shape):
        self.__set_shape(shape)
        self.minx = float(min(self.shape[::3]))
        self.miny = float(min(self.shape[1::3]))
        self.minz = float(min(self.shape[2::3]))

        self.maxx = float(max(self.shape[::3]))
        self.maxy = float(max(self.shape[1::3]))
        self.maxz = float(max(self.shape[2::3]))

        self.centerx = float((self.minx + self.maxx) / 2.0)
        self.centery = float((self.miny + self.maxy) / 2.0)
        self.centerz = float((self.minz + self.maxz) / 2.0)

        if self.useOrtho:
            self.model_translation_z = 2.5 * (self.maxz - self.minz)
            near = self.maxz - self.minz
            far = self.maxz + self.model_translation_z
        else:
            self.model_translation_z = 2.5 * (self.maxz - self.minz)
            near = self.maxz - self.minz
            far = self.maxz + self.model_translation_z

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()

        if self.useOrtho:
            dist = max((self.maxy - self.miny), (self.maxx - self.minx)) / 1.75
            glOrtho(-dist * self.aspectRatio, dist * self.aspectRatio, \
                    -dist, dist, \
                    near, far)
        else:
            gluPerspective(self.fieldOfView, self.aspectRatio, near, far)

        glViewport(0, 0, self.width, self.height)
        self.display_shape(use_vbo=True)

        buffer = glReadPixels(0, 0, self.width, self.height, GL_RGB, GL_UNSIGNED_BYTE)
        image = Image.frombytes(mode="RGB", size=(self.width, self.height), data=buffer)
        image = image.transpose(Image.FLIP_TOP_BOTTOM)
        self.last_render = image
        return image


    def display_shape(self, use_vbo=False):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        glTranslatef(0, 0, -self.model_translation_z)

        camera_angle = uniform(self.minAngle, self.maxAngle)
        # camera_trans = 0.0
        glRotatef(camera_angle, 0., 1., 0.)
        # glTranslatef(0.0, 0.0, camera_trans)

        if not use_vbo:
            for indecies in self.tl:
                glBegin(GL_TRIANGLES)
                for i in indecies:
                    glColor3f( \
                        self.texture[3*i] / 255.,     \
                        self.texture[3*i + 1] / 255., \
                        self.texture[3*i + 2] / 255.)
                    glVertex3f( \
                        self.shape[3*(i-1)],     \
                        self.shape[3*(i-1) + 1], \
                        self.shape[3*(i-1) + 2])
                glEnd()
        else:
            self.texture_vbo.bind()
            glEnableClientState(GL_COLOR_ARRAY)
            glColorPointer(3, GL_FLOAT, 0, None)

            self.shape_vbo.bind()
            glEnableClientState(GL_VERTEX_ARRAY)
            glVertexPointer(3, GL_FLOAT, 0, None)

            self.tl_vbo.bind()
            glDrawElements(GL_TRIANGLES, 3*len(self.tl), GL_UNSIGNED_INT, None)

            glDisableClientState(GL_COLOR_ARRAY)
            glDisableClientState(GL_VERTEX_ARRAY)

            self.tl_vbo.unbind()
            self.shape_vbo.unbind()
            self.texture_vbo.unbind()


    # def __del__(self):
    #     print "Bye-bye!"
    #     glutDestroyWindow(self.win)


    def __dist(self, a, b):
        return np.linalg.norm(a.ravel() - b.ravel())


    def __find_closest_idx(self, point3D):
        min_dist = self.__dist(point3D, self.shape[0:3])
        min_idx  = 0
        for i in xrange(1, self.vertex_count):
            d = self.__dist(point3D, self.shape[(3*i):(3*i + 3)])
            if d < min_dist:
                min_dist = d
                min_idx  = i
        return min_idx


    def __find_k_closest_idx(self, point3D, k):
        mins = []
        for i in xrange(k):
            mins.append((-1, float("inf")))

        for i in xrange(0, self.vertex_count):
            d = self.__dist(point3D, self.shape[(3*i):(3*i + 3)])
            max_tuple = max(mins, key=lambda (_, dist): dist)
            if d < max_tuple[1]:
                index = mins.index(max_tuple)
                mins[index] = (i, d)

        return map(lambda (index, dist): index, mins)


    def __unproject(self, winX, winY):
        modelview  = glGetDoublev(GL_MODELVIEW_MATRIX)
        projection = glGetDoublev(GL_PROJECTION_MATRIX)
        viewport   = glGetIntegerv(GL_VIEWPORT)

        winX = float(winX)
        winY = float(viewport[3] - winY)
        winZ = glReadPixels(int(winX), int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT)[0][0]
        posX, posY, posZ = gluUnProject(winX, winY, winZ, modelview, projection, viewport)
        return np.array([posX, posY, posZ])


    def model_indecies(self, landmarks):
        indecies = []

        inx_projections = []
        cv_projections = []

        modelview  = glGetDoublev(GL_MODELVIEW_MATRIX)
        projection = glGetDoublev(GL_PROJECTION_MATRIX)
        viewport   = glGetIntegerv(GL_VIEWPORT)

        for winX, winY in landmarks:
            print "Finding index for: ", (winX, winY),

            if self.useOrtho:
                point3D = self.__unproject(winX, winY)
                indecies.append(self.__find_closest_idx(point3D))
            else:
                _, _, indx = self.find_triangle_for(winX, winY, use_k=1000)
                indecies.append(indx)

            # test found index
            if self.useOrtho:
                error = 3.0
            else:
                error = 10.0

            inx = indecies[-1]
            x, y, z = \
                gluProject(self.shape[3*inx], self.shape[3*inx + 1], self.shape[3*inx + 2],
                           modelview, projection, viewport)
            y = viewport[3] - y
            inx_projections.append((x, y))
            assert winX - error <= x <= winX + error, \
                "X failed in test: index\n %f %f %f %f" % (x, y, winX, winY)
            assert winY - error <= y <= winY + error, "Y failed in test: index"
            print ".... test passed"

            # focal length test
            print "ModelView:\n", modelview
            print "Projection:\n", np.array(projection).T
            print "ViewPort:\n", viewport
            print "ViewPort*Projection", np.array(viewport).dot(np.array(projection))

            print "T:\n", self.T

            fW, fH = self.get_focal_length()
            print "f:\n", fW, fH

            if self.useOrtho:
                C = np.array([ \
                    [fW,  0., 0.], \
                    [0., fH , 0. ], \
                    [0., 0.,  0.]
                    ])
            else:
                C = np.array([ \
                    [fW,  0., self.width / 2.], \
                    [0., fH , self.height / 2. ], \
                    [0., 0., 1.]
                    ])
            print "C:\n", C

            R = self.get_glrotation_matrix()
            print "R:\n", R
            R = self.T.dot(R)
            print "TR:\n", R

            t = self.get_gltranslation_vector()
            print "t:\n", t
            t = self.T.dot(t)
            print "Tt:\n", t

            X = np.array([self.shape[3*inx], self.shape[3*inx + 1], self.shape[3*inx + 2]]).ravel()
            print "X_model:\n", X

            X_cam = R.dot(X) + t
            print "X_cam:\n", X_cam

            X_proj = C.dot(X_cam)
            print "X_proj:\n", X_proj

            if self.useOrtho:
                # x_p = (self.width  / 2) * X_proj[0] + (self.width  / 2)
                # y_p = (self.height / 2) * X_proj[1] + (self.height / 2)
                x_p = X_proj[0] + (self.width  / 2)
                y_p = X_proj[1] + (self.height / 2)
            else:
                x_p = X_proj[0]/X_proj[2]
                y_p = X_proj[1]/X_proj[2]

            print x_p, y_p, winX, winY
            assert winX - error <= x_p <= winX + error, "X failed in test: focal length"
            assert winY - error <= y_p <= winY + error, "Y failed in test: focal length"
            cv_projections.append((x_p, y_p))
            print ".... test passed"
            raw_input()

        # DRAWINGS
        img = Image.fromarray(misc.imread("/Users/dmitrypetuhov/Desktop/diploma/code/experiments/avg_face.png"))
        draw = ImageDraw.Draw(img)
        r, color = 1, (255, 0, 0)
        for (x, y) in inx_projections:
            draw.ellipse((x-r, y-r, x+r, y+r), fill=color)
        misc.imsave("/Users/dmitrypetuhov/Desktop/diploma/code/experiments/avg_face_pts.png", img)

        img = Image.fromarray(misc.imread("/Users/dmitrypetuhov/Desktop/diploma/code/experiments/avg_face_pts.png"))
        draw = ImageDraw.Draw(img)
        r, color = 1, (0, 0, 0)
        for (x, y) in cv_projections:
            draw.ellipse((x-r, y-r, x+r, y+r), fill=color)
        misc.imsave("/Users/dmitrypetuhov/Desktop/diploma/code/experiments/avg_face_pts.png", img)

        return indecies


    def __get_bary(self, P, P1, P2, P3):
        u = P2 - P1
        v = P3 - P1
        n = np.cross(P1, P2)
        w = P - P1

        gamma = np.true_divide(np.cross(u, w).dot(n), n.dot(n))
        beta = np.true_divide(np.cross(w, v).dot(n), n.dot(n))
        alpha = 1.0 - gamma - beta

        print "P", P
        print "Projection", alpha*P1 + beta*P2 + gamma*P3
        if (0 <= alpha <= 1) and (0 <= beta <= 1) and (0 <= gamma <= 1):
            return alpha, beta, gamma
        else:
            return None


    def find_triangle_for(self, u, v, use_k=2):
        if self.useOrtho:
            print "ORTHO IS NOT YET UNSUPPORTED"
            return
        fW, fH = self.get_focal_length()
        C = np.array([ \
            [fW,  0., self.width / 2.], \
            [0., fH , self.height / 2. ], \
            [0., 0., 1.]
        ])
        invC = np.linalg.inv(C)
        coords = np.array([u, v, 1])
        R = self.get_rotation_matrix()
        t = self.get_translation_vector()

        coords_nrm = invC.dot(coords)
        origin     = -(R.T).dot(t)
        direction  = (R.T).dot(coords_nrm)
        # print "RAY(origin, direction)", origin, direction

        min_t   = float("inf")
        bary    = {'alpha': -1.0, 'beta': -1.0, 'gamma': -1.0}
        closest_idx = -1

        # Find the closest triangles
        point3D = self.__unproject(u, v)
        k_idx = self.__find_k_closest_idx(point3D, use_k)
        rows = []
        for idx in k_idx:
            rows += list(np.where(np.any(self.tl == idx, axis=-1))[0])

        for row in rows:
            id1, id2, id3 = self.tl[row]

            a = np.array([self.shape[3*id1], self.shape[3*id1 + 1], self.shape[3*id1 + 2]]).ravel()
            b = np.array([self.shape[3*id2], self.shape[3*id2 + 1], self.shape[3*id2 + 2]]).ravel()
            c = np.array([self.shape[3*id3], self.shape[3*id3 + 1], self.shape[3*id3 + 2]]).ravel()
            A = np.array(\
                [[a[0] - b[0], a[0] - c[0], direction[0]],\
                 [a[1] - b[1], a[1] - c[1], direction[1]],\
                 [a[2] - b[2], a[2] - c[2], direction[2]]])
            B = np.array([a[0] - origin[0], a[1] - origin[1], a[2] - origin[2]])
            beta, gamma, t = np.linalg.solve(A, B)
            if t > 0.0 and (0.0 <= gamma <= 1.0) and (0.0 <= beta <= 1.0 - gamma) and t < min_t:
                min_t = t
                bary['alpha'] = 1.0 - beta - gamma
                bary['beta'] = beta
                bary['gamma'] = gamma
                indices = [id1, id2, id3]

                p = bary['alpha'] * a + beta * b + gamma * c
                tmp = np.array([np.linalg.norm(p - a), np.linalg.norm(p - b), np.linalg.norm(p - c)])
                closest_idx = indices[np.where(tmp == np.min(tmp))[0][0]]

                print "Good triangle!"

        print "---" * 10,
        print "SEARCH ENDED"
        assert indices != []
        assert bary['alpha'] != -1.0

        return bary, indices, closest_idx


    def get_glrotation_matrix(self):
        modelview = glGetDoublev(GL_MODELVIEW_MATRIX)
        return np.array(modelview[:3, :3]).T


    def get_gltranslation_vector(self):
        modelview = glGetDoublev(GL_MODELVIEW_MATRIX)
        return np.array(modelview[3, :3])


    def get_rotation_matrix(self):
        return self.T.dot(self.get_glrotation_matrix())


    def get_translation_vector(self):
        return self.T.dot(self.get_gltranslation_vector())


    def project_bary_point(self, id1, id2, id3, alpha, beta, gamma):
        R = self.get_rotation_matrix()
        t = self.get_translation_vector()

        X = np.array([self.shape[3*int(id1)], self.shape[3*int(id1) + 1], self.shape[3*int(id1) + 2]]).ravel()
        Y = np.array([self.shape[3*int(id2)], self.shape[3*int(id2) + 1], self.shape[3*int(id2) + 2]]).ravel()
        Z = np.array([self.shape[3*int(id3)], self.shape[3*int(id3) + 1], self.shape[3*int(id3) + 2]]).ravel()

        P      = alpha * X + beta * Y + gamma * Z
        P_cam  = R.dot(P) + t

        fW, fH = self.get_focal_length()

        if self.useOrtho:
            C = np.array([ \
                    [fW,  0., 0.], \
                    [0., fH , 0. ], \
                    [0., 0.,  0.]
                    ])
            P_proj = C.dot(P_cam)
            x_p = P_proj[0] + (self.width  / 2)
            y_p = P_proj[1] + (self.height / 2)
        else:
            C = np.array([ \
                [fW,  0., self.width / 2.], \
                [0., fH , self.height / 2. ], \
                [0., 0., 1.]
            ])
            P_proj = C.dot(P_cam)
            x_p = P_proj[0] / P_proj[2]
            y_p = P_proj[1] / P_proj[2]

        return x_p, y_p


if __name__ == "__main__":
    print "Nothing to do here..."
